# Changelog

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

## Version 0.10.0

Major refactoring. 

* Fixed the storage of metrics in annotations instead of memory cache (so that metrics survive after restart).
* Refactor of the status field of the Device Server and Databaseds resource types. means an interface change and a new api version for the tango group kind. Webhooks are now enabled together with the [cert-manager](https://artifacthub.io/packages/helm/cert-manager/cert-manager) for the certificates. The subchart could be disabled when deploying into stfc techops cluster (or any other cluster). In those case, certificates must be created before the chart is installed and injected with a mutation (i.e. kyverno) in the caBoundle of the webhook client-config  
* Enabled webhook
* Enabled cert-manager for caBoundle injection for the webhook (new dependency)
* Removed certificates creations from Makefile because cert-manager is enabled. Secrets from ska-tango-ping are created in the k8s-pre-install-chart. 
* New annotations for the DS in order to store the running configuration in the TangoDB. 

New DeviceServer Status structure: 
```
status:
  devicecount: 1
  devicereadycount: 1
  devices:
  - class: PowerSupply
    name: test/power_supply/1
    ping: 251
    retTime: "2024-11-26T14:22:26Z"
    state: STANDBY
    status: The device is in STANDBY state.
  replicas: 1
  resources:
    clusterip: 10.107.116.230
    dsservice: ds-powersupply-test
    lbs:
    - ip: 192.168.49.99
    ports:
    - name: tango-server
      port: 45450
    - name: tango-heartbeat
      port: 45460
    - name: tango-event
      port: 45470
    servicetype: LoadBalancer
  state: Running
  succeeded: 1
```

New Databaseds Status structure:
```
status:
    ds: 1 of 1
    replicas: 2
    resources:
      databaseds:
        clusterip: 10.106.29.244
        dsservice: databaseds-tango-base-test
        lbs:
        - ip: 192.168.49.97
        ports:
        - name: ds
          port: 10000
        servicetype: LoadBalancer
      tangodb:
        clusterip: 10.106.98.131
        dsservice: databaseds-tangodb-databaseds-tango-base-test
        ports:
        - name: mysql
          port: 3306
        servicetype: ClusterIP
    state: Running
    succeeded: 2
    tangodb: 1 of 1
```