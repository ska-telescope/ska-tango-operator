# ska-tango-operator

This project is an extension of k8s with a Custom Resource Definition (CRD i.e. Device Server and Databaseds) and a Controller to give custom behaviours in order to have a better usage of TANGO-controls in kubernetes.

## Installation

This project is structured to use k8s for development and testing so that the build environment, test environment and test results are all completely reproducible and are independent of host environment. It uses ``make`` to provide a consistent UI (run ``make help`` for targets documentation).

### Install docker

Follow the instructions available at [here](https://docs.docker.com/engine/install/ubuntu/).

### Install minikube

You will need to install `minikube` or equivalent k8s installation in order to set up your test environment. You can follow the instruction at [here](https://gitlab.com/ska-telescope/sdi/deploy-minikube/):
```
git clone git@gitlab.com:ska-telescope/sdi/deploy-minikube.git
cd deploy-minikube
make all
eval $(minikube docker-env)
```

*Please note that the command `eval $(minikube docker-env)` will point your local docker client at the docker-in-docker for minikube. Use this only for building the docker image and another shell for other work.*

### Install GO

Follow the instructions available at [here](https://go.dev/doc/install).

## How to Use

Install the operator with the following command: 

```
$ git clone git@gitlab.com:ska-telescope/ska-tango-operator.git
$ cd ska-tango-operator
$ git submodule update --init --recursive
$ make k8s-install-chart

```

The operator works in collaboration with [the ska-tango-ping service](https://gitlab.com/ska-telescope/ska-tango-ping) which is installed (the needed certificate is created in the make target `k8s-pre-install-chart`.

It also requires [cert-manager](https://github.com/cert-manager/cert-manager) for provisioning the certificates for the webhook server. 

Install the example with the following command:

```
$ kubectl apply -f config/samples/all-in-one.yml

```

Check what's happening with the following commands:

```
$ kubectl get deviceserver
$ kubectl get databaseds

```

Uninstall the examples and the operator with the following commands:

```
$ kubectl delete -f config/samples/all-in-one.yml
$ make k8s-uninstall-chart

```

### Generate the charts

The charts available in this repository are generated with the following commands:

```
$ make helm-operator
$ make helm-crd

```

## External references and other information

* This project has been made with the help of an operator SDK called [kubebuilder](  https://github.com/kubernetes-sigs/kubebuilder) written in GO. 
* The databaseds is a Device server as any other and technically can be defined as device server. 
