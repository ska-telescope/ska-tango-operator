/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package v1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// DatabaseDSSpec defines the desired state of DatabaseDS
type DatabaseDSSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	//+kubebuilder:validation:Default=false

	// Enable Load Balancer for external access to DatabaseDS
	// +optional
	EnableLoadBalancer bool `json:"enableLoadBalancer,omitempty"`

	//+kubebuilder:validation:Default=false

	// Disable Network Policies
	// +optional
	DisablePolicies bool `json:"disablepolicies,omitempty"`

	//+kubebuilder:validation:Default=false

	// Enable the creation of a PV for the DatabaseDS
	// +optional
	UsePV bool `json:"usePV,omitempty"`

	// DatabaseDS  resource name that is the Tango DatabaseDS the DS will run as: optional, defaults to Name
	DatabaseDS string `json:"databaseds,omitempty"`

	//+kubebuilder:validation:MinLength=0
	//+kubebuilder:validation:default=artefact.skao.int/ska-tango-images-tango-databaseds-alpine:0.3.0

	// Source image to deploy DatabaseDS from - default: artefact.skao.int/ska-tango-images-tango-databaseds-alpine:0.3.0, registry.gitlab.com/tango-controls/docker/tango-db:5.16
	DDSImage string `json:"dsimage,omitempty"`

	// DatabaseDS TC config that get's loaded at DS boot - FQ file name, HTTP URL, or full config body .json: optional
	Config string `json:"config,omitempty"`

	//+kubebuilder:validation:MinLength=0
	//+kubebuilder:validation:default=artefact.skao.int/ska-tango-images-tango-db:10.4.14

	// Source image to deploy Tango DB from - default: artefact.skao.int/ska-tango-images-tango-db:10.4.14, registry.gitlab.com/tango-controls/docker/mysql:5.16-mysql-8-1
	DBImage string `json:"dbimage,omitempty"`

	// Kubernetes cluster domain default cluster.local
	//+kubebuilder:default=cluster.local
	//+optional
	ClusterDomain string `json:"clusterDomain,omitempty"`

	// Pull Policy for image - default: IfNotPresent
	//+optional
	ImagePullPolicy string `json:"imagePullPolicy,omitempty"`

	// OmniOrb Port default 10000
	//+kubebuilder:validation:Minimum=0
	//+kubebuilder:default=10000
	//+optional
	Port int32 `json:"orbport,omitempty"`

	//+kubebuilder:validation:MinLength=0
	//+kubebuilder:validation:Default=tango

	// Tango DB DB name - default: tango
	DBDb string `json:"dbdb,omitempty"`

	//+kubebuilder:validation:MinLength=0
	//+kubebuilder:validation:Default=secret

	// Tango DB Root Password - default: secret
	DBRootPw string `json:"dbrootpw,omitempty"`

	//+kubebuilder:validation:MinLength=0
	//+kubebuilder:validation:Default=tango

	// Tango DB User - default: tango
	DBUser string `json:"dbuser,omitempty"`

	//+kubebuilder:validation:MinLength=0
	//+kubebuilder:validation:Default=tango

	// Tango DB User Password - default: tango
	DBPassword string `json:"dbpassword,omitempty"`

	//TangoDB StorageClass - default: standard
	//+optional
	TangoDBStorageClass string `json:"tangoDBStorageClass,omitempty"`

	// Specifies the Volumes.
	//+optional
	Volumes []corev1.Volume `json:"volumes,omitempty"`

	// Specifies the VolumeMounts.
	//+optional
	VolumeMounts []corev1.VolumeMount `json:"volumeMounts,omitempty"`

	// Specifies the Environment variables.
	//+optional
	Env []corev1.EnvVar `json:"env,omitempty"`

	// Specifies the Pull Secrets.
	//+optional
	PullSecrets []corev1.LocalObjectReference `json:"imagePullSecrets,omitempty"`

	// Specifies the NodeSelector configuration.
	//+optional
	NodeSelector map[string]string `json:"nodeSelector,omitempty"`

	// Specifies the Affinity configuration.
	//+optional
	Affinity *corev1.Affinity `json:"affinity,omitempty"`

	// Specifies the Toleration configuration.
	//+optional
	Tolerations []corev1.Toleration `json:"tolerations,omitempty"`

	// Specifies the Environment variables.
	//+optional
	Resources *corev1.ResourceRequirements `json:"resources,omitempty"`
}

// DatabaseDSStatus defines the observed state of DatabaseDS
type DatabaseDSStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Replicas  int32  `json:"replicas"`
	Succeeded int32  `json:"succeeded"`
	State     string `json:"state"`
	Resources string `json:"resources"`
	TangoDB   string `json:"tangodb"`
	DS        string `json:"ds"`
}

//+kubebuilder:object:root=true

// DatabaseDS is the Schema for the databaseds API
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:path="databaseds"
// +kubebuilder:printcolumn:name="Components",type="integer",JSONPath=".status.replicas",description="The number of Components Requested in the DatabaseDS",priority=0
// +kubebuilder:printcolumn:name="Succeeded",type="integer",JSONPath=".status.succeeded",description="The number of Components Launched in the DatabaseDS",priority=0
// +kubebuilder:printcolumn:name="Age",type="date",JSONPath=".metadata.creationTimestamp",description="The number of Components Requested in the DatabaseDS",priority=0
// +kubebuilder:printcolumn:name="State",type="string",JSONPath=".status.state",description="Status of the DatabaseDS",priority=0
// +kubebuilder:printcolumn:name="TangoDB",type="string",JSONPath=".status.tangodb",description=" TangoDB details of the DatabaseDS",priority=1
// +kubebuilder:printcolumn:name="DS",type="string",JSONPath=".status.ds",description=" DS details of the DatabaseDS",priority=1
// +kubebuilder:printcolumn:name="Resources",type="string",JSONPath=".status.resources",description=" Resource details of the DatabaseDS",priority=1
type DatabaseDS struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DatabaseDSSpec   `json:"spec,omitempty"`
	Status DatabaseDSStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// DatabaseDSList contains a list of DatabaseDS
type DatabaseDSList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []DatabaseDS `json:"items"`
}

func init() {
	SchemeBuilder.Register(&DatabaseDS{}, &DatabaseDSList{})
}

func (*DatabaseDS) Hub() {}
