/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
// +kubebuilder:docs-gen:collapse=Apache License
// +kubebuilder:webhook:path=/mutate-analytics-tango-controls-org-v1-databaseds,mutating=true,failurePolicy=fail,groups=tango.tango-controls.org,resources=DatabaseDS,verbs=create;update,versions=v1,name=mdatabaseds-v1.tango-controls.org,admissionReviewVersions=v1,sideEffects=None
// +kubebuilder:webhook:verbs=create;update,path=/validate-analytics-tango-controls-org-v1-databaseds,mutating=false,failurePolicy=fail,groups=tango.tango-controls.org,resources=DatabaseDS,versions=v1,name=vdatabaseds-v1.tango-controls.org,admissionReviewVersions=v1,sideEffects=None

// Go imports
package v1

import (
	"context"
	"fmt"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	validationutils "k8s.io/apimachinery/pkg/util/validation"
	"k8s.io/apimachinery/pkg/util/validation/field"

	ctrl "sigs.k8s.io/controller-runtime"

	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

var databasedslog = ctrl.Log.WithName("databaseds-resource")

func (r *DatabaseDS) SetupWebhookWithManager(mgr ctrl.Manager) error {
	databasedslog.Info("Activating Webhook")
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		WithDefaulter(&DatabaseDSCustomDefaulter{}).
		WithValidator(&DatabaseDSCustomValidator{}).
		Complete()
}

var _ webhook.Defaulter = &DatabaseDS{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *DatabaseDS) Default() {
	databasedslog.Info("default", "name", r.Name)

	if r.Spec.DDSImage == "" {
		r.Spec.DDSImage = "artefact.skao.int/ska-tango-images-tango-databaseds-alpine:0.3.0"
	}

	if r.Spec.DBImage == "" {
		r.Spec.DBImage = "artefact.skao.int/ska-tango-images-tango-db:10.4.14"
	}

	if r.Spec.ImagePullPolicy == "" {
		r.Spec.ImagePullPolicy = "IfNotPresent"
	}

	if r.Spec.ClusterDomain == "" {
		r.Spec.ClusterDomain = "cluster.local"
	}

	if r.Spec.Port == 0 {
		r.Spec.Port = 10000
	}

	if r.Spec.DatabaseDS == "" {
		r.Spec.DatabaseDS = r.Name
	}

	// if r.Spec.Daemon == nil {
	// 	r.Spec.Daemon = new(bool)
	// }

	// if r.Spec.Replicas == nil {
	// 	r.Spec.Replicas = new(int32)
	// 	*r.Spec.Replicas = 1
	// }

	// if r.Spec.Replicas == 0 {
	// 	r.Spec.Replicas = int32(5)
	// }
}

// TODO(user): change verbs to "verbs=create;update;delete" if you want to enable deletion validation.

// var _ webhook.Validator = &DatabaseDS{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *DatabaseDS) ValidateCreate() error {
	databasedslog.Info("validate create", "name", r.Name)

	return r.validateDatabaseDS()
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *DatabaseDS) ValidateUpdate(old runtime.Object) error {
	databasedslog.Info("validate update", "name", r.Name)

	return r.validateDatabaseDS()
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *DatabaseDS) ValidateDelete() error {
	databasedslog.Info("validate delete", "name", r.Name)

	// TODO(user): fill in your validation logic upon object deletion.
	return nil
}

func (r *DatabaseDS) validateDatabaseDS() error {
	var allErrs field.ErrorList
	if err := r.validateDatabaseDSName(); err != nil {
		allErrs = append(allErrs, err)
	}
	if err := r.validateDatabaseDSSpec(); err != nil {
		allErrs = append(allErrs, err)
	}
	if len(allErrs) == 0 {
		return nil
	}

	return apierrors.NewInvalid(
		schema.GroupKind{Group: "tango.tango-controls.org", Kind: "DatabaseDS"},
		r.Name, allErrs)
}

func (r *DatabaseDS) validateDatabaseDSSpec() *field.Error {
	// The field helpers from the kubernetes API machinery help us return nicely
	// structured validation errors.
	// return validateScheduleFormat(
	// 	r.Spec.Schedule,
	// 	field.NewPath("spec").Child("schedule"))
	return nil
}

func (r *DatabaseDS) validateDatabaseDSName() *field.Error {
	if len(r.ObjectMeta.Name) > validationutils.DNS1035LabelMaxLength-11 {
		// The job name length is 63 character like all Kubernetes objects
		// (which must fit in a DNS subdomain). The cronjob controller appends
		// a 11-character suffix to the cronjob (`-$TIMESTAMP`) when creating
		// a job. The job name length limit is 63 characters. Therefore cronjob
		// names must have length <= 63-11=52. If we don't validate this here,
		// then job creation will fail later.
		return field.Invalid(field.NewPath("metadata").Child("name"), r.Name, "must be no more than 52 characters")
	}
	return nil
}

type DatabaseDSCustomDefaulter struct {
}

var _ webhook.CustomDefaulter = &DatabaseDSCustomDefaulter{}

/*
We use the `webhook.CustomDefaulter`interface to set defaults to our CRD.
A webhook will automatically be served that calls this defaulting.

The `Default`method is expected to mutate the receiver, setting the defaults.
*/

// Default implements webhook.CustomDefaulter so a webhook will be registered for the Kind CronJob.
func (d *DatabaseDSCustomDefaulter) Default(ctx context.Context, obj runtime.Object) error {
	databaseDS, ok := obj.(*DatabaseDS)

	if !ok {
		return fmt.Errorf("expected an DatabaseDS object but got %T", obj)
	}
	databasedslog.Info("Defaulting for CronJob", "name", databaseDS.GetName())

	// Set default values
	d.applyDefaults(databaseDS)
	return nil
}

// applyDefaults applies default values to CronJob fields.
func (d *DatabaseDSCustomDefaulter) applyDefaults(databaseds *DatabaseDS) {
	databaseds.Default()
}

type DatabaseDSCustomValidator struct {
	// TODO(user): Add more fields as needed for validation
}

var _ webhook.CustomValidator = &DatabaseDSCustomValidator{}

// ValidateCreate implements webhook.CustomValidator so a webhook will be registered for the type CronJob.
func (v *DatabaseDSCustomValidator) ValidateCreate(ctx context.Context, obj runtime.Object) (admission.Warnings, error) {
	databaseds, ok := obj.(*DatabaseDS)
	if !ok {
		return nil, fmt.Errorf("expected a DatabaseDS object but got %T", obj)
	}
	databasedslog.Info("Validation for DatabaseDS upon creation", "name", databaseds.GetName())

	return nil, databaseds.ValidateCreate()
}

// ValidateUpdate implements webhook.CustomValidator so a webhook will be registered for the type CronJob.
func (v *DatabaseDSCustomValidator) ValidateUpdate(ctx context.Context, oldObj, newObj runtime.Object) (admission.Warnings, error) {
	databaseds, ok := newObj.(*DatabaseDS)
	if !ok {
		return nil, fmt.Errorf("expected a DatabaseDS object but got %T", newObj)
	}
	databasedslog.Info("Validation for DatabaseDS upon creation", "name", databaseds.GetName())

	return nil, databaseds.ValidateUpdate(oldObj)
}

// ValidateDelete implements webhook.CustomValidator so a webhook will be registered for the type CronJob.
func (v *DatabaseDSCustomValidator) ValidateDelete(ctx context.Context, obj runtime.Object) (admission.Warnings, error) {
	databaseDS, ok := obj.(*DatabaseDS)
	if !ok {
		return nil, fmt.Errorf("expected a DatabaseDS object but got %T", obj)
	}
	databasedslog.Info("Validation for DatabaseDS upon deletion", "name", databaseDS.GetName())

	// TODO(user): fill in your validation logic upon object deletion.

	return nil, databaseDS.ValidateDelete()
}
