/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package v2

import (
	"fmt"
	"strconv"
	"strings"

	tangov1 "gitlab.com/ska-telescope/ska-tango-operator/api/v1"
	"sigs.k8s.io/controller-runtime/pkg/conversion"
)

/*
ConvertTo is expected to modify its argument to contain the converted object.
Most of the conversion is straightforward copying, except for converting our changed field.
*/
// ConvertTo converts this DatabaseDS to the Hub version (v1).
func (src *DatabaseDS) ConvertTo(dstRaw conversion.Hub) error {
	dds := dstRaw.(*tangov1.DatabaseDS)
	dds.ObjectMeta = src.ObjectMeta
	dds.Spec = tangov1.DatabaseDSSpec(src.Spec)
	dds.Status = tangov1.DatabaseDSStatus{
		Replicas:  src.Status.Replicas,
		Succeeded: src.Status.Succeeded,
		State:     src.Status.State,
		DS:        src.Status.DS,
		TangoDB:   src.Status.TangoDB,
		Resources: "",
	}
	if src.Status.Resources.TangoDB.ServiceName == "" {
		return nil
	}
	var tangodb_ports []string
	for i := 0; i < len(src.Status.Resources.TangoDB.Ports); i++ {
		tangodb_ports = append(tangodb_ports, fmt.Sprintf("%s/%d", src.Status.Resources.TangoDB.Ports[i].Name, src.Status.Resources.TangoDB.Ports[i].Port))
	}
	var tango_db_lbs []string
	for i := 0; i < len(src.Status.Resources.TangoDB.Lbs); i++ {
		tango_db_lbs = append(tango_db_lbs, src.Status.Resources.TangoDB.Lbs[i].IP)
	}
	tango_db_lbs_str := ""
	if len(tango_db_lbs) > 0 {
		tango_db_lbs_str = fmt.Sprintf(" LB: %s", strings.Join(tango_db_lbs[:], ","))
	}
	tangodb_res := fmt.Sprintf("%s := %s/%s, %s%s", src.Status.Resources.TangoDB.ServiceName, src.Status.Resources.TangoDB.ServiceType, src.Status.Resources.TangoDB.ClusterIP, strings.Join(tangodb_ports, ","), tango_db_lbs_str)

	var dds_ports []string
	for i := 0; i < len(src.Status.Resources.DatabaseDS.Ports); i++ {
		dds_ports = append(dds_ports, fmt.Sprintf("%s/%d", src.Status.Resources.DatabaseDS.Ports[i].Name, src.Status.Resources.DatabaseDS.Ports[i].Port))
	}
	var dds_lbs []string
	for i := 0; i < len(src.Status.Resources.DatabaseDS.Lbs); i++ {
		dds_lbs = append(dds_lbs, src.Status.Resources.DatabaseDS.Lbs[i].IP)
	}
	dds_lbs_str := ""
	if len(dds_lbs) > 0 {
		dds_lbs_str = fmt.Sprintf(" LB: %s", strings.Join(dds_lbs[:], ","))
	}
	dds_res := fmt.Sprintf("%s := %s/%s, %s%s", src.Status.Resources.DatabaseDS.ServiceName, src.Status.Resources.DatabaseDS.ServiceType, src.Status.Resources.DatabaseDS.ClusterIP, strings.Join(dds_ports, ","), dds_lbs_str)

	dds.Status.Resources = fmt.Sprintf("%s - %s", tangodb_res, dds_res)

	return nil
}

/*
ConvertFrom is expected to modify its receiver to contain the converted object.
Most of the conversion is straightforward copying, except for converting our changed field.
*/

// ConvertFrom converts from the Hub version (v1) to this version.
func (dds *DatabaseDS) ConvertFrom(srcRaw conversion.Hub) error {
	src := srcRaw.(*tangov1.DatabaseDS)
	dds.ObjectMeta = src.ObjectMeta
	dds.Spec = DatabaseDSSpec(src.Spec)
	dds.Status = DatabaseDSStatus{
		Replicas:  src.Status.Replicas,
		Succeeded: src.Status.Succeeded,
		State:     src.Status.State,
		DS:        src.Status.DS,
		TangoDB:   src.Status.TangoDB,
		Resources: DatabaseDSResources{},
	}
	if len(src.Status.Resources) == 0 {
		return nil
	}
	// databaseds-tangodb-databaseds-tango-base-test := ClusterIP/10.100.221.165, mysql/3306 - databaseds-tango-base-test := LoadBalancer/10.104.0.120, ds/10000 LB: 192.168.49.97
	parts := strings.Split(src.Status.Resources, " - ")
	if len(parts) < 2 {
		return nil
	}
	// databaseds-tangodb-databaseds-tango-base-test := ClusterIP/10.100.221.165, mysql/3306
	tangodb_parts := strings.Split(parts[0], " := ")
	if len(tangodb_parts) < 2 {
		return nil
	}
	tangodb_svc_info_parts := strings.Split(tangodb_parts[1], " LB: ")
	if len(tangodb_svc_info_parts) < 1 {
		return nil
	}
	tangodb_svc_info_cluster_ip_ports_parts := strings.Split(tangodb_svc_info_parts[0], ", ")
	var ports []Ports
	if len(tangodb_svc_info_cluster_ip_ports_parts) > 1 {
		ports_parts := strings.Split(tangodb_svc_info_cluster_ip_ports_parts[1], ",")
		for i := 0; i < len(ports_parts); i++ {
			tangodb_svc_info_onlyports_parts := strings.Split(ports_parts[i], "/")
			if len(tangodb_svc_info_onlyports_parts) < 2 {
				continue
			}
			port, _ := strconv.ParseInt(tangodb_svc_info_onlyports_parts[1], 10, 32)
			new_port := Ports{
				Name: tangodb_svc_info_onlyports_parts[0],
				Port: int32(port),
			}
			ports = append(ports, new_port)
		}
	}
	var tangolbs []LB
	if len(tangodb_svc_info_parts) > 1 {
		lbs_list := strings.Split(tangodb_svc_info_parts[1], ",")
		for i := 0; i < len(lbs_list); i++ {
			lb := LB{
				IP: lbs_list[i],
			}
			tangolbs = append(tangolbs, lb)
		}
	}
	tandodbres := Resources{
		ServiceName: tangodb_parts[0],
		ServiceType: strings.Split(tangodb_svc_info_cluster_ip_ports_parts[0], "/")[0],
		ClusterIP:   strings.Split(tangodb_svc_info_cluster_ip_ports_parts[0], "/")[1],
		Ports:       ports,
		Lbs:         tangolbs,
	}

	//databaseds-tango-base-test := LoadBalancer/10.104.0.120, ds/10000 LB: 192.168.49.97
	dds_parts := strings.Split(parts[1], " := ")
	if len(dds_parts) < 2 {
		return nil
	}
	dds_svc_info_parts := strings.Split(dds_parts[1], " LB: ")
	if len(dds_svc_info_parts) < 1 {
		return nil
	}
	dds_svc_info_cluster_ip_ports_parts := strings.Split(dds_svc_info_parts[0], ", ")
	if len(dds_svc_info_cluster_ip_ports_parts) < 2 {
		return nil
	}

	dds_svc_info_cluster_ip_onlyports_parts := strings.Split(dds_svc_info_cluster_ip_ports_parts[1], ",")
	var dds_ports []Ports
	for i := 0; i < len(dds_svc_info_cluster_ip_onlyports_parts); i++ {
		ports_part := strings.Split(dds_svc_info_cluster_ip_onlyports_parts[i], "/")
		if len(ports_part) < 2 {
			continue
		}
		port, _ := strconv.ParseInt(ports_part[1], 10, 32)
		new_port := Ports{
			Name: ports_part[0],
			Port: int32(port),
		}
		dds_ports = append(dds_ports, new_port)
	}
	var lbs []LB
	if len(dds_svc_info_parts) > 1 {
		lbs_list := strings.Split(dds_svc_info_parts[1], ",")
		for i := 0; i < len(lbs_list); i++ {
			lb := LB{
				IP: lbs_list[i],
			}
			lbs = append(lbs, lb)
		}
	}

	dds_res := Resources{
		ServiceName: dds_parts[0],
		ServiceType: strings.Split(dds_svc_info_cluster_ip_ports_parts[0], "/")[0],
		ClusterIP:   strings.Split(dds_svc_info_cluster_ip_ports_parts[0], "/")[1],
		Ports:       dds_ports,
		Lbs:         lbs,
	}
	dds.Status.Resources = DatabaseDSResources{
		TangoDB:    tandodbres,
		DatabaseDS: dds_res,
	}
	return nil
}
