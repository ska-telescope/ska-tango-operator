/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package v2

import (
	"fmt"
	"strconv"
	"strings"

	tangov1 "gitlab.com/ska-telescope/ska-tango-operator/api/v1"
	"sigs.k8s.io/controller-runtime/pkg/conversion"
)

/*
ConvertTo is expected to modify its argument to contain the converted object.
Most of the conversion is straightforward copying, except for converting our changed field.
*/
// ConvertTo converts this DatabaseDS to the Hub version (v1).
func (src *DeviceServer) ConvertTo(dstRaw conversion.Hub) error {
	dds := dstRaw.(*tangov1.DeviceServer)
	dds.ObjectMeta = src.ObjectMeta
	dds.Spec = tangov1.DeviceServerSpec(src.Spec)
	dds.Status = tangov1.DeviceServerStatus{
		Replicas:  src.Status.Replicas,
		Succeeded: src.Status.Succeeded,
		State:     src.Status.State,
		Resources: "",
	}
	if src.Status.Resources.ServiceName == "" {
		return nil
	}

	// "DSService: <service_name> := <ServiceType>/<ClusterIP>, <Port name>/<Port number> LB: <ip>"
	var ports []string
	for i := 0; i < len(src.Status.Resources.Ports); i++ {
		ports = append(ports, fmt.Sprintf("%s/%d", src.Status.Resources.Ports[i].Name, src.Status.Resources.Ports[i].Port))
	}
	var lbs []string
	for i := 0; i < len(src.Status.Resources.Lbs); i++ {
		lbs = append(lbs, src.Status.Resources.Lbs[i].IP)
	}
	lbs_str := fmt.Sprintf(" LB: %s", strings.Join(lbs[:], ","))
	if len(lbs) == 0 {
		lbs_str = ""
	}
	dds.Status.Resources = fmt.Sprintf("DSService: %s := %s/%s, %s%s", src.Status.Resources.ServiceName, src.Status.Resources.ServiceType, src.Status.Resources.ClusterIP, strings.Join(ports, ","), lbs_str)
	return nil
}

/*
ConvertFrom is expected to modify its receiver to contain the converted object.
Most of the conversion is straightforward copying, except for converting our changed field.
*/

// ConvertFrom converts from the Hub version (v1) to this version.
func (dds *DeviceServer) ConvertFrom(srcRaw conversion.Hub) error {
	src := srcRaw.(*tangov1.DeviceServer)
	dds.ObjectMeta = src.ObjectMeta
	dds.Spec = DeviceServerSpec(src.Spec)
	dds.Status = DeviceServerStatus{
		Replicas:  src.Status.Replicas,
		Succeeded: src.Status.Succeeded,
		State:     src.Status.State,
		Resources: Resources{},
	}
	if len(src.Status.Resources) == 0 {
		return nil
	}
	// "DSService: <service_name> := <ServiceType>/<ClusterIP>, <Port name>/<Port number> LB: <ip>"
	parts := strings.Split(src.Status.Resources, " := ")
	if len(parts) < 2 {
		return nil
	}
	svc_name := strings.Split(parts[0], ": ")[1]
	lbs_split := strings.Split(parts[1], "LB: ")
	if len(lbs_split) < 1 {
		return nil
	}
	service_type_cluster_ip_ports_lb_str := strings.Split(lbs_split[0], ", ")
	if len(service_type_cluster_ip_ports_lb_str) < 2 {
		return nil
	}
	svc_type_cluster_ip := strings.Split(service_type_cluster_ip_ports_lb_str[0], "/")
	if len(svc_type_cluster_ip) < 2 {
		return nil
	}
	service_type := svc_type_cluster_ip[0]
	cluster_ip := svc_type_cluster_ip[1]
	ports_parts := strings.Split(service_type_cluster_ip_ports_lb_str[1], ",")
	var ports []Ports
	if len(ports_parts) > 0 {
		for i := 0; i < len(ports_parts); i++ {
			onlyports_parts := strings.Split(ports_parts[i], "/")
			if len(onlyports_parts) < 2 {
				continue
			}
			port, _ := strconv.ParseInt(onlyports_parts[1], 10, 32)
			new_port := Ports{
				Name: onlyports_parts[0],
				Port: int32(port),
			}
			ports = append(ports, new_port)
		}
	}
	var lbs []LB
	if len(lbs_split) > 1 {
		lbs_list := strings.Split(lbs_split[1], ",")
		for i := 0; i < len(lbs_list); i++ {
			lb := LB{
				IP: lbs_list[i],
			}
			lbs = append(lbs, lb)
		}
	}
	resource := Resources{
		ServiceName: svc_name,
		ServiceType: service_type,
		ClusterIP:   cluster_ip,
		Ports:       ports,
		Lbs:         lbs,
	}
	dds.Status.Resources = resource
	return nil
}
