/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package v2

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// DeviceServerSpec defines the desired state of DeviceServer
type DeviceServerSpec struct {
	//+kubebuilder:validation:Default=false

	// Disable Network Policies
	//+optional
	DisablePolicies bool `json:"disablepolicies,omitempty"`

	// DeviceServer databaseds resource name that is the Tango DatabaseDS the DeviceServer will run against: mandatory
	DatabaseDS string `json:"databaseds"`

	// External DeviceServer DNS name or IP address - must be supplied if ExternalDeviceServer is set
	// Make the deviceServer an external device only - this will only deploy the Service type ExternalName and pass DS config to TangoDB
	// +optional
	ExternalDeviceServer string `json:"externalDeviceServer,omitempty"`

	//+kubebuilder:validation:Default=false

	// Enable Load Balancer for external access to DatabaseDS
	// +optional
	EnableLoadBalancer bool `json:"enableLoadBalancer,omitempty"`

	//+kubebuilder:validation:Default=false

	// Specify Direct Connection to TANGO_HOST in DatabaseDS config option
	//+optional
	DirectConnection bool `json:"directConnection,omitempty"`

	//+kubebuilder:validation:Minimum=0

	// Number of workers to spawn - default will be 5
	//+optional
	// Replicas int32 `json:"replicas,omitempty"`

	// DeviceServer config for this job - FQ file name, HTTP URL, or full config body .json: mandatory
	Config string `json:"config"`

	// DeviceServer name: mandatory
	DSName string `json:"dsname"`

	// DeviceServer script for this job - FQ file name or full script body either .py or .ipynb: mandatory
	//+optional
	Script string `json:"script"`

	// DeviceServer script for this job - FQ file name or full script body either .py or .ipynb: mandatory
	//+optional
	Command string `json:"command"`

	// DeviceServer lifecycle PostStart
	//+optional
	PostStart string `json:"postStart"`

	// DeviceServer lifecycle PreStop
	//+optional
	PreStop string `json:"preStop"`

	//+kubebuilder:validation:MinLength=0

	// Arguments to pass to app boot - default:
	Args string `json:"args,omitempty"`

	//+kubebuilder:validation:MinLength=0
	//+kubebuilder:validation:Default=artefact.skao.int/ska-tango-images-tango-pytango-alpine:0.3.1

	// Source image to deploy cluster from - default: artefact.skao.int/ska-tango-images-tango-pytango-alpine:0.3.1
	Image string `json:"image,omitempty"`

	// Pull Policy for image - default: IfNotPresent
	//+optional
	ImagePullPolicy string `json:"imagePullPolicy,omitempty"`

	// Specifies the DeviceServers that must be started before this one.
	//+optional
	DependsOn []string `json:"dependsOn,omitempty"`

	// OmniOrb IP default false
	//+kubebuilder:default=false
	//+optional
	LegacyCompatibility bool `json:"legacycompatibility,omitempty"`

	// OmniOrb Port default 45450
	//+kubebuilder:validation:Minimum=0
	//+kubebuilder:default=45450
	//+optional
	OrbPort int32 `json:"orbport,omitempty"`

	// ZMQ Heartbeat Port default 45460
	//+kubebuilder:validation:Minimum=0
	//+kubebuilder:default=45460
	//+optional
	HeartbeatPort int32 `json:"heartbeatport,omitempty"`

	// Kubernetes cluster domain default cluster.local
	//+kubebuilder:default=cluster.local
	//+optional
	ClusterDomain string `json:"clusterDomain,omitempty"`

	// ZMQ Event Port default 45470
	//+kubebuilder:validation:Minimum=0
	//+kubebuilder:default=45470
	//+optional
	EventPort int32 `json:"eventport,omitempty"`

	// Specifies the Volumes.
	//+optional
	Volumes []corev1.Volume `json:"volumes,omitempty"`

	// Specifies the VolumeMounts.
	//+optional
	VolumeMounts []corev1.VolumeMount `json:"volumeMounts,omitempty"`

	// Specifies the Environment variables.
	//+optional
	Env []corev1.EnvVar `json:"env,omitempty"`

	// Specifies the Pull Secrets.
	//+optional
	PullSecrets []corev1.LocalObjectReference `json:"imagePullSecrets,omitempty"`

	// Specifies the NodeSelector configuration.
	//+optional
	NodeSelector map[string]string `json:"nodeSelector,omitempty"`

	// Specifies the Affinity configuration.
	//+optional
	Affinity *corev1.Affinity `json:"affinity,omitempty"`

	// Specifies the Toleration configuration.
	//+optional
	Tolerations []corev1.Toleration `json:"tolerations,omitempty"`

	// Specifies the Environment variables.
	//+optional
	Resources *corev1.ResourceRequirements `json:"resources,omitempty"`
}

// Describe the device inside a DeviceServer
type Device struct {

	// Name of the device
	Name string `json:"name"`

	// Ping, in milliseconds of the device
	Ping int32 `json:"ping"`

	// State of the device (Command: State())
	State string `json:"state"`

	// Status of the device (Command: Status())
	Status string `json:"status"`

	// Class of the device (Command: Status())
	Class string `json:"class"`

	// Retrieval time of this information
	RetTime metav1.Time `json:"retTime"`
}

// Describe a container port (name and number)
type Ports struct {
	Name string `json:"name,omitempty"`
	Port int32  `json:"port,omitempty"`
}

// Describe a LB Ip assigned
type LB struct {
	IP string `json:"ip,omitempty"`
}

// Describe the resources allocated per DeviceServer
type Resources struct {

	// Service Name available
	ServiceName string `json:"dsservice,omitempty"`

	// Service Type available
	ServiceType string `json:"servicetype,omitempty"`

	// ClusterIP assigned
	ClusterIP string `json:"clusterip,omitempty"`

	// Ports allocated
	Ports []Ports `json:"ports,omitempty"`

	// Load balancer allocated
	Lbs []LB `json:"lbs,omitempty"`
}

// DeviceServerStatus defines the observed state of DeviceServer
type DeviceServerStatus struct {

	// Replicas number of the statefulset
	Replicas int32 `json:"replicas"`

	// Succeeded number of the statefulset
	Succeeded int32 `json:"succeeded"`

	// Number of devices available in the DeviceServer
	DeviceCount int32 `json:"devicecount,omitempty"`

	// Number of devices answering to the State() command
	DeviceReadyCount int32 `json:"devicereadycount,omitempty"`

	// Devices list
	Devices []Device `json:"devices,omitempty"`

	// State of the DeviceServer
	State string `json:"state"`

	// Resources of the DeviceServer
	Resources Resources `json:"resources,omitempty"`
}

// DeviceServer is the Schema for the deviceservers API
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +versionName=v1
// +kubebuilder:storageversion
// +kubebuilder:printcolumn:name="Components",type="integer",JSONPath=".status.replicas",description="The number of Components Requested in the DeviceServer",priority=0
// +kubebuilder:printcolumn:name="Succeeded",type="integer",JSONPath=".status.succeeded",description="The number of Components Launched in the DeviceServer",priority=0
// +kubebuilder:printcolumn:name="Age",type="date",JSONPath=".metadata.creationTimestamp",description="The number of Components Requested in the DeviceServer",priority=0
// +kubebuilder:printcolumn:name="State",type="string",JSONPath=".status.state",description="Status of the DeviceServer",priority=0
// +kubebuilder:printcolumn:name="Resources",type="string",JSONPath=".status.resources",description=" Resource details of the DeviceServer",priority=1
type DeviceServer struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DeviceServerSpec   `json:"spec,omitempty"`
	Status DeviceServerStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// DeviceServerList contains a list of DeviceServer
type DeviceServerList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []DeviceServer `json:"items"`
}

func init() {
	SchemeBuilder.Register(&DeviceServer{}, &DeviceServerList{})
}
