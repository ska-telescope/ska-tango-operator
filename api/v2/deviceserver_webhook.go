/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
// +kubebuilder:docs-gen:collapse=Apache License
// +kubebuilder:webhook:path=/mutate-tango-tango-controls-org-v2-deviceserver,mutating=true,failurePolicy=fail,groups=tango.tango-controls.org,resources=DeviceServer,verbs=create;update,versions=v2,name=mdeviceserver-v2.tango-controls.org,admissionReviewVersions=v1,sideEffects=None
// +kubebuilder:webhook:verbs=create;update,path=/validate-analytics-tango-controls-org-v2-deviceserver,mutating=false,failurePolicy=fail,groups=tango.tango-controls.org,resources=DeviceServer,versions=v2,name=vdeviceserver.tango-controls.org,admissionReviewVersions=v1,sideEffects=None

// Go imports
package v2

import (
	"context"
	"fmt"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	validationutils "k8s.io/apimachinery/pkg/util/validation"
	"k8s.io/apimachinery/pkg/util/validation/field"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

var deviceserverlog = ctrl.Log.WithName("ds-resource")

func (r *DeviceServer) SetupWebhookWithManager(mgr ctrl.Manager) error {
	deviceserverlog.Info("Activating Webhook")
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		WithDefaulter(&DeviceServerCustomDefaulter{}).
		WithValidator(&DeviceServerCustomValidator{}).
		Complete()
}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *DeviceServer) Default() {
	deviceserverlog.Info("default", "name", r.Name)

	if r.Spec.Image == "" {
		r.Spec.Image = "tangocs/tango-pytango:latest"
	}

	if r.Spec.ImagePullPolicy == "" {
		r.Spec.ImagePullPolicy = "IfNotPresent"
	}

	if r.Spec.OrbPort == 0 {
		r.Spec.OrbPort = 45450
	}

	if r.Spec.ClusterDomain == "" {
		r.Spec.ClusterDomain = "cluster.local"
	}

	if r.Spec.HeartbeatPort == 0 {
		r.Spec.HeartbeatPort = 45460
	}

	if r.Spec.EventPort == 0 {
		r.Spec.EventPort = 45470
	}
}

type DeviceServerCustomDefaulter struct {
}

var _ webhook.CustomDefaulter = &DeviceServerCustomDefaulter{}

/*
We use the `webhook.CustomDefaulter`interface to set defaults to our CRD.
A webhook will automatically be served that calls this defaulting.

The `Default`method is expected to mutate the receiver, setting the defaults.
*/

// Default implements webhook.CustomDefaulter so a webhook will be registered for the Kind CronJob.
func (d *DeviceServerCustomDefaulter) Default(ctx context.Context, obj runtime.Object) error {
	deviceserver, ok := obj.(*DeviceServer)

	if !ok {
		return fmt.Errorf("expected an DeviceServer object but got %T", obj)
	}
	deviceserverlog.Info("Defaulting for CronJob", "name", deviceserver.GetName())

	// Set default values
	d.applyDefaults(deviceserver)
	return nil
}

// applyDefaults applies default values to CronJob fields.
func (d *DeviceServerCustomDefaulter) applyDefaults(deviceserver *DeviceServer) {
	deviceserver.Default()
}

type DeviceServerCustomValidator struct {
	// TODO(user): Add more fields as needed for validation
}

var _ webhook.CustomValidator = &DeviceServerCustomValidator{}

// ValidateCreate implements webhook.CustomValidator so a webhook will be registered for the type CronJob.
func (v *DeviceServerCustomValidator) ValidateCreate(ctx context.Context, obj runtime.Object) (admission.Warnings, error) {
	deviceserver, ok := obj.(*DeviceServer)
	if !ok {
		return nil, fmt.Errorf("expected a DeviceServer object but got %T", obj)
	}
	deviceserverlog.Info("Validation for DeviceServer upon creation", "name", deviceserver.GetName())

	return nil, deviceserver.ValidateCreate()
}

// ValidateUpdate implements webhook.CustomValidator so a webhook will be registered for the type CronJob.
func (v *DeviceServerCustomValidator) ValidateUpdate(ctx context.Context, oldObj, newObj runtime.Object) (admission.Warnings, error) {
	deviceserver, ok := newObj.(*DeviceServer)
	if !ok {
		return nil, fmt.Errorf("expected a DeviceServer object but got %T", newObj)
	}
	deviceserverlog.Info("Validation for DeviceServer upon creation", "name", deviceserver.GetName())

	return nil, deviceserver.ValidateUpdate(oldObj)
}

// ValidateDelete implements webhook.CustomValidator so a webhook will be registered for the type CronJob.
func (v *DeviceServerCustomValidator) ValidateDelete(ctx context.Context, obj runtime.Object) (admission.Warnings, error) {
	deviceserver, ok := obj.(*DeviceServer)
	if !ok {
		return nil, fmt.Errorf("expected a DeviceServer object but got %T", obj)
	}
	deviceserverlog.Info("Validation for DatabaseDS upon deletion", "name", deviceserver.GetName())

	// TODO(user): fill in your validation logic upon object deletion.

	return nil, deviceserver.ValidateDelete()
}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *DeviceServer) ValidateCreate() error {
	deviceserverlog.Info("validate create", "name", r.Name)

	return r.validateDeviceServer()
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *DeviceServer) ValidateUpdate(old runtime.Object) error {
	deviceserverlog.Info("validate update", "name", r.Name)

	return r.validateDeviceServer()
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *DeviceServer) ValidateDelete() error {
	deviceserverlog.Info("validate delete", "name", r.Name)

	// TODO(user): fill in your validation logic upon object deletion.
	return nil
}

func (r *DeviceServer) validateDeviceServer() error {
	var allErrs field.ErrorList
	if err := r.validateDeviceServerName(); err != nil {
		allErrs = append(allErrs, err)
	}
	if err := r.validateDeviceServerSpec(); err != nil {
		allErrs = append(allErrs, err)
	}
	if len(allErrs) == 0 {
		return nil
	}

	return apierrors.NewInvalid(
		schema.GroupKind{Group: "tango.tango-controls.org", Kind: "DeviceServer"},
		r.Name, allErrs)
}

func (r *DeviceServer) validateDeviceServerSpec() *field.Error {
	// The field helpers from the kubernetes API machinery help us return nicely
	// structured validation errors.
	// return validateScheduleFormat(
	// 	r.Spec.Schedule,
	// 	field.NewPath("spec").Child("schedule"))
	return nil
}

func (r *DeviceServer) validateDeviceServerName() *field.Error {
	if len(r.ObjectMeta.Name) > validationutils.DNS1035LabelMaxLength-11 {
		// The job name length is 63 character like all Kubernetes objects
		// (which must fit in a DNS subdomain). The cronjob controller appends
		// a 11-character suffix to the cronjob (`-$TIMESTAMP`) when creating
		// a job. The job name length limit is 63 characters. Therefore cronjob
		// names must have length <= 63-11=52. If we don't validate this here,
		// then job creation will fail later.
		return field.Invalid(field.NewPath("metadata").Child("name"), r.Name, "must be no more than 52 characters")
	}
	return nil
}
