/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package v2

import (
	"context"
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	tangov1 "gitlab.com/ska-telescope/ska-tango-operator/api/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

var resource_name = "testds"
var test1_name = resource_name + "basic"

var _ = Context("Inside of a new namespace", func() {
	ctx := context.TODO()
	ns := SetupTest(ctx)
	var (
		key              types.NamespacedName
		created, fetched *DeviceServer
	)
	Describe("when no existing resources exist", func() {

		It("should create a new DeviceServer resource with the specified name", func() {
			key = types.NamespacedName{
				Name:      test1_name,
				Namespace: ns.Name,
			}
			created = &DeviceServer{
				ObjectMeta: metav1.ObjectMeta{
					Name:      test1_name,
					Namespace: ns.Name,
				},
				Spec: DeviceServerSpec{
					DatabaseDS:      "testds",
					Image:           "registry.gitlab.com/ska-telescope/ska-tango-operator/tango-examples:0.1.6",
					ImagePullPolicy: "IfNotPresent",
					Env: []corev1.EnvVar{{Name: "x",
						Value: "y"}},
				},
			}

			By("creating an API obj")
			Expect(k8sClient.Create(ctx, created)).To(Succeed())

			fetched = &DeviceServer{}
			// fetched.Spec.Env[]
			Expect(k8sClient.Get(ctx, key, fetched)).To(Succeed())
			Expect(fetched).To(Equal(created))

			// check webhook functions
			By("validating the DeviceServer resource attributes")
			Expect(created.ValidateCreate()).To(BeNil())
			Expect(created.ValidateUpdate(fetched)).To(BeNil())
			Expect(created.validateDeviceServerSpec()).To(BeNil())
			// good name
			By("validating a good name")
			Expect(created.validateDeviceServerName()).To(BeNil())
			// bad name
			By("validating a bad name")
			fetched.ObjectMeta.Name = randStringRunes(52 + 1)
			Expect(fetched.validateDeviceServerName()).Should(HaveOccurred())
			By("Setting defaults")
			fetched.Spec.ImagePullPolicy = ""
			Expect(fetched.Spec.ImagePullPolicy).To(Equal(""))
			fetched.Default()
			Expect(fetched.Spec.ImagePullPolicy).To(Equal("IfNotPresent"))

			By("deleting the created object")
			Expect(k8sClient.Delete(ctx, created)).To(Succeed())
			Expect(k8sClient.Get(ctx, key, created)).ToNot(Succeed())

			By("Doing a Copy - deep compare")
			copy := fetched.DeepCopy()
			Expect(copy).To(BeEquivalentTo(fetched))
		})
	})
})

var _ = Context("Test DeviceServer corversion", func() {
	ctx := context.TODO()
	ns := SetupTest(ctx)
	var (
		created    *tangov1.DeviceServer
		convert_v2 *DeviceServer
		convert_v1 *tangov1.DeviceServer
	)
	Describe("when no existing resources exist", func() {

		It("From a tango v1 DeviceServer", func() {
			created = &tangov1.DeviceServer{
				ObjectMeta: metav1.ObjectMeta{
					Name:      test1_name,
					Namespace: ns.Name,
				},
				Spec: tangov1.DeviceServerSpec{
					DatabaseDS:      "testds",
					Image:           "registry.gitlab.com/ska-telescope/ska-tango-operator/tango-examples:0.1.6",
					ImagePullPolicy: "IfNotPresent",
					Env: []corev1.EnvVar{{Name: "x",
						Value: "y"}},
				},
				Status: tangov1.DeviceServerStatus{
					Replicas:  1,
					Succeeded: 1,
					Resources: "DSService: ds-powerswitch-pdu := ClusterIP/10.101.154.234, tango-server/45450,tango-heartbeat/45460,tango-event/45470",
					State:     "Running",
				},
			}
			convert_v2 = &DeviceServer{}
			By("converting to v2")
			Expect(convert_v2.ConvertFrom(created)).To(Succeed())
			Expect(convert_v2.Status.Resources.ServiceName).To(Equal("ds-powerswitch-pdu"))
			Expect(convert_v2.Status.Resources.ServiceType).To(Equal("ClusterIP"))
			Expect(convert_v2.Status.Resources.ClusterIP).To(Equal("10.101.154.234"))
			Expect(len(convert_v2.Status.Resources.Ports)).To(Equal(3))
			Expect(convert_v2.Status.Resources.Ports[0].Name).To(Equal("tango-server"))
			Expect(convert_v2.Status.Resources.Ports[1].Name).To(Equal("tango-heartbeat"))
			Expect(convert_v2.Status.Resources.Ports[2].Name).To(Equal("tango-event"))
			Expect(convert_v2.Status.Resources.Ports[0].Port).To(Equal(int32(45450)))
			Expect(convert_v2.Status.Resources.Ports[1].Port).To(Equal(int32(45460)))
			Expect(convert_v2.Status.Resources.Ports[2].Port).To(Equal(int32(45470)))
			convert_v1 = &tangov1.DeviceServer{}
			By("converting to v1")
			Expect(convert_v2.ConvertTo(convert_v1)).To(Succeed())
			Expect(convert_v1.Status.Resources).To(Equal(created.Status.Resources))
		})
	})
})

var _ = Context("Test Databaseds corversion", func() {
	ctx := context.TODO()
	ns := SetupTest(ctx)
	var (
		created    *tangov1.DatabaseDS
		convert_v2 *DatabaseDS
		convert_v1 *tangov1.DatabaseDS
	)
	Describe("when no existing resources exist", func() {

		It("From a tango v1 DatabaseDS", func() {
			created = &tangov1.DatabaseDS{
				ObjectMeta: metav1.ObjectMeta{
					Name:      test1_name,
					Namespace: ns.Name,
				},
				Spec: tangov1.DatabaseDSSpec{
					DatabaseDS: "testds",
				},
				Status: tangov1.DatabaseDSStatus{
					Replicas:  1,
					Succeeded: 1,
					Resources: "databaseds-tangodb-databaseds-tango-base-test := ClusterIP/10.100.221.165, mysql/3306 - databaseds-tango-base-test := LoadBalancer/10.104.0.120, ds/10000 LB: 192.168.49.97",
					State:     "Running",
				},
			}
			convert_v2 = &DatabaseDS{}
			By("converting to v2")
			Expect(convert_v2.ConvertFrom(created)).To(Succeed())
			Expect(convert_v2.Status.Resources.TangoDB.ServiceName).To(Equal("databaseds-tangodb-databaseds-tango-base-test"))
			Expect(convert_v2.Status.Resources.TangoDB.ServiceType).To(Equal("ClusterIP"))
			Expect(convert_v2.Status.Resources.TangoDB.ClusterIP).To(Equal("10.100.221.165"))
			Expect(len(convert_v2.Status.Resources.TangoDB.Ports)).To(Equal(1))
			Expect(convert_v2.Status.Resources.TangoDB.Ports[0].Name).To(Equal("mysql"))
			Expect(convert_v2.Status.Resources.TangoDB.Ports[0].Port).To(Equal(int32(3306)))
			Expect(len(convert_v2.Status.Resources.TangoDB.Lbs)).To(Equal(0))

			Expect(convert_v2.Status.Resources.DatabaseDS.ServiceName).To(Equal("databaseds-tango-base-test"))
			Expect(convert_v2.Status.Resources.DatabaseDS.ServiceType).To(Equal("LoadBalancer"))
			Expect(convert_v2.Status.Resources.DatabaseDS.ClusterIP).To(Equal("10.104.0.120"))
			Expect(len(convert_v2.Status.Resources.DatabaseDS.Ports)).To(Equal(1))
			Expect(convert_v2.Status.Resources.DatabaseDS.Ports[0].Name).To(Equal("ds"))
			Expect(convert_v2.Status.Resources.DatabaseDS.Ports[0].Port).To(Equal(int32(10000)))
			Expect(len(convert_v2.Status.Resources.DatabaseDS.Lbs)).To(Equal(1))
			Expect(convert_v2.Status.Resources.DatabaseDS.Lbs[0].IP).To(Equal("192.168.49.97"))
			convert_v1 = &tangov1.DatabaseDS{}
			By("converting to v1")
			Expect(convert_v2.ConvertTo(convert_v1)).To(Succeed())
			fmt.Println(convert_v1.Status.Resources)
			fmt.Println(created.Status.Resources)
			Expect(convert_v1.Status.Resources).To(Equal(created.Status.Resources))
		})
	})
})
