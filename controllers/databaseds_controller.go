/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package controllers

import (
	"reflect"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"context"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	tangov2 "gitlab.com/ska-telescope/ska-tango-operator/api/v2"
	dtypes "gitlab.com/ska-telescope/ska-tango-operator/types"
)

var (
	databaseDSOwnerKey = ".metadata.databasedscontroller"
)

// DatabaseDSReconciler reconciles a DatabaseDS object
type DatabaseDSReconciler struct {
	client.Client
	Log       logr.Logger
	CustomLog dtypes.CustomLogger
	Scheme    *runtime.Scheme
	Recorder  record.EventRecorder
}

// +kubebuilder:rbac:groups=tango.tango-controls.org,resources=databaseds,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=tango.tango-controls.org,resources=databaseds/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=core,resources=persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=events,verbs=get;list;watch;create;update;patch;delete

func (r *DatabaseDSReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("databaseds", req.NamespacedName)
	clog := r.CustomLog.WithValues("databaseds", req.NamespacedName)
	_ = clog

	var databaseds tangov2.DatabaseDS
	if err := r.Get(ctx, req.NamespacedName, &databaseds); err != nil {
		if client.IgnoreNotFound(err) == nil {
			Debugf(log, "unable to fetch DatabaseDS(delete in progress?): "+err.Error())
			not_found_error.WithLabelValues(databaseds.GetName(), databaseds.Namespace).Inc()
			reconcile_failures.WithLabelValues(databaseds.GetName(), databaseds.Namespace).Inc()
			// we'll ignore not-found errors, since they can't be fixed by an immediate
			// requeue (we'll need to wait for a new notification), and we cannot get them
			// on deleted requests.
			return ctrl.Result{}, client.IgnoreNotFound(err)
		}
	}
	Infof(log, "INITIAL Status replicas: %d, succeeded: %d", databaseds.Status.Replicas, databaseds.Status.Succeeded)

	databaseds.Status.Replicas = 0
	databaseds.Status.Succeeded = 0
	databaseds.Status.Resources = tangov2.DatabaseDSResources{}
	databaseds.Status.State = "Building"

	var childStatefulSets appsv1.StatefulSetList
	if err := r.List(ctx, &childStatefulSets, client.InNamespace(req.Namespace), client.MatchingFields{databaseDSOwnerKey: req.Name}); err != nil {
		log.Error(err, "unable to list child StatefulSets")
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}
	}

	var (
		childSS      int = 0
		childSSready int = 0
	)
	// make sure that the children exist and that the status is correct
	for _, sschld := range childStatefulSets.Items {
		childSS += 1
		if sschld.Status.ReadyReplicas > 0 {
			childSSready += 1
		}
	}

	Debugf(log, "incoming context: %+v", databaseds)

	// setup configuration.
	dcontext := dtypes.DSSetConfig(databaseds)
	dcontext.Metadata = databaseds.ObjectMeta

	// Get resource details
	tangodbres, databasedsres, tangodbState, dsState, err := r.resourceDetails(dcontext)
	if err != nil {
		databaseds.Status.State = err.Error()
		if _ = r.Status().Update(ctx, &databaseds); err != nil {
			Errorf(log, err, "unable to update DatabaseDS status: %s", req.Name)
		}
		return ctrl.Result{}, err
	}
	databaseds.Status.Resources = tangov2.DatabaseDSResources{
		TangoDB:    tangodbres,
		DatabaseDS: databasedsres,
	}
	databaseds.Status.TangoDB = tangodbState
	databaseds.Status.DS = dsState
	Debugf(log, "Current Status: %+v", databaseds.Status)

	// Generate desired children.

	// process the ConfigMap
	if res, err := r.checkSetConfigMap(ctx, req, &dcontext, &databaseds); err != nil {
		if _ = r.Status().Update(ctx, &databaseds); err != nil {
			Errorf(log, err, "unable to update DatabaseDS status: %s", req.Name)
		}
		return res, err
	}

	// process the TangoDB StatefulSet
	if res, err := r.checkSetTangoDB(ctx, req, &dcontext, &databaseds); err != nil {
		if _ = r.Status().Update(ctx, &databaseds); err != nil {
			Errorf(log, err, "unable to update DatabaseDS status: %s", req.Name)
		}
		return res, err
	}

	// process the DatabaseDS StatefulSet
	// the TangoDB must be up and running
	if databaseds.Status.Succeeded > 0 {
		if res, err := r.checkSetDatabaseDS(ctx, req, &dcontext, &databaseds); err != nil {
			if _ = r.Status().Update(ctx, &databaseds); err != nil {
				Errorf(log, err, "unable to update DatabaseDS status: %s", req.Name)
			}
			return res, err
		}
	}

	// Compute status based on latest observed state. Check the running child StatefulSets
	// databaseds.Status.Replicas is automatically incremented when each of the StatefulSets are Ready
	// (don't know how this magic happens???)
	if databaseds.Status.Replicas == 2 &&
		databaseds.Status.Replicas == databaseds.Status.Succeeded &&
		childSS == 2 && childSSready == childSS {
		databaseds.Status.State = "Running"
	}
	Infof(log, "Status replicas: %d, succeeded: %d - Status: %s", databaseds.Status.Replicas, databaseds.Status.Succeeded, databaseds.Status.State)

	return ctrl.Result{}, r.Status().Update(ctx, &databaseds)
}

func (r *DatabaseDSReconciler) SetupWithManager(mgr ctrl.Manager) error {

	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &appsv1.StatefulSet{}, databaseDSOwnerKey, func(rawObj client.Object) []string {
		// grab the StatefulSet object, extract the owner...
		statefulset := rawObj.(*appsv1.StatefulSet)
		owner := metav1.GetControllerOf(statefulset)
		if owner == nil {
			return nil
		}
		// ...make sure it's a DatabaseDS ...
		if !IsValidVersion(owner.APIVersion) || owner.Kind != "DatabaseDS" {
			return nil
		}

		// ...and if so, return it
		return []string{owner.Name}
	}); err != nil {
		return err
	}

	log := r.Log.WithValues("databaseds", "DeleteEvent")
	return ctrl.NewControllerManagedBy(mgr).
		For(&tangov2.DatabaseDS{}).
		Owns(&appsv1.StatefulSet{}).
		Owns(&corev1.Service{}).
		Owns(&corev1.ServiceAccount{}).
		Owns(&corev1.ConfigMap{}).
		WithEventFilter(predicate.Funcs{
			DeleteFunc: func(e event.DeleteEvent) bool {
				// The reconciler adds a finalizer so we perform clean-up
				// when the delete timestamp is added
				// add your code for deleting zombie ServiceAccounts here
				// if reflect.TypeOf(e.Object).String() == "*v1.ServiceAccount" || reflect.TypeOf(e.Object).String() == "*v1.DatabaseDS" {
				Debugf(log, "****** DeleteEvent: %s/%s/%s", reflect.TypeOf(e.Object).String(), e.Object.GetNamespace(), e.Object.GetName())
				// }

				// Suppress Delete events to avoid filtering them out in the Reconcile function
				return false
			},
		}).
		Complete(r)

}
