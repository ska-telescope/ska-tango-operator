/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package controllers

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net"
	"reflect"
	"regexp"
	"strconv"
	"time"

	"context"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"

	// networkingv1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"

	"github.com/go-logr/logr"
	"github.com/go-test/deep"
	"github.com/patrickmn/go-cache"
	"github.com/prometheus/client_golang/prometheus"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/metrics"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"gitlab.com/ska-telescope/ska-tango-operator/utils"

	tangov2 "gitlab.com/ska-telescope/ska-tango-operator/api/v2"
	dtypes "gitlab.com/ska-telescope/ska-tango-operator/types"
)

var (
	deviceServerOwnerKey = ".metadata.deviceservercontroller"
	minWaitMilliSecs     = 1000
	retriesBeforeSleep   = 25
)

var (
	not_found_error = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "not_found_error",
			Help: "Unable to fetch DeviceServer.",
		},
		[]string{"device_server_name", "namespace"},
	)
	not_found_databaseds = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "not_found_databaseds",
			Help: "Unable to fetch Databaseds.",
		},
		[]string{"device_server_name", "namespace"},
	)
	not_found_statefulset = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "not_found_statefulset",
			Help: "unable to list child StatefulSets.",
		},
		[]string{"device_server_name", "namespace"},
	)
	invalid_configuration = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "invalid_configuration",
			Help: "DeviceServer config is invalid.",
		},
		[]string{"device_server_name", "namespace"},
	)
	config_job_failed = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "config_job_failed",
			Help: "DeviceServerJob script is invalid.",
		},
		[]string{"device_server_name", "namespace"},
	)
	tempfile_allocate_problem = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "tempfile_allocate_problem",
			Help: "Unable to allocate tempfile for json2tango.",
		},
		[]string{"device_server_name", "namespace"},
	)
	tempfile_write_problem = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "tempfile_write_problem",
			Help: "Unable to write to tempfile for json2tango.",
		},
		[]string{"device_server_name", "namespace"},
	)
	tempfile_close_problem = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "tempfile_close_problem",
			Help: "Unable to close tempfile for json2tango.",
		},
		[]string{"device_server_name", "namespace"},
	)
	pending_databaseds = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "pending_databaseds",
			Help: "Reconcile pending Databaseds.",
		},
		[]string{"device_server_name", "namespace"},
	)
	pending_dependency = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "pending_dependency",
			Help: "Pending dependency on device name.",
		},
		[]string{"device_server_name", "namespace", "dependency_device_name"},
	)
	config_applied = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "config_applied",
			Help: "Config applied return code on json2tango.",
		},
		[]string{"device_server_name", "namespace"},
	)
	reconcile_count = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "reconcile_count",
			Help: "Number of reconcile made.",
		},
		[]string{"device_server_name", "namespace"},
	)
	reconcile_count_after_success = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "reconcile_count_after_success",
			Help: "Number of reconcile made after success.",
		},
		[]string{"device_server_name", "namespace"},
	)
	startup_time = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "startup_time",
			Help: "StatefulSet Startup time.",
		},
		[]string{"device_server_name", "namespace"},
	)
	reconcile_failures = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "reconcile_failures",
			Help: "Number of reconcile failures.",
		},
		[]string{"device_server_name", "namespace"},
	)
	script_setup_failed = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "script_setup_failed",
			Help: "Number of script setup failures.",
		},
		[]string{"device_server_name", "namespace"},
	)
	image_pulling = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "image_pulling",
			Help: "Waiting time in seconds for pulling the image of the pod.",
		},
		[]string{"device_server_name", "namespace"},
	)
	databaseds_wait_time = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "databaseds_wait_time",
			Help: "Waiting time in seconds for the Databaseds.",
		},
		[]string{"device_server_name", "namespace"},
	)
	dependson_wait_time = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "dependson_wait_time",
			Help: "Waiting time in seconds for the device server dependencies.",
		},
		[]string{"device_server_name", "namespace"},
	)
	setupdb_wait_time = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "setupdb_wait_time",
			Help: "Waiting time in seconds for TangoDB initialization.",
		},
		[]string{"device_server_name", "namespace"},
	)
)

var validDeviceFQDN = regexp.MustCompile(`^([\w\-\.]+:\d+\/)?[\w\-\.]+\/[\w\-\.]+\/[\w\-\.]+`)
var validHostPort = regexp.MustCompile(`([A-Za-z0-9_.])+:\d+`)

// +kubebuilder:rbac:groups=tango.tango-controls.org,resources=deviceservers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=tango.tango-controls.org,resources=deviceservers/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=apps,resources=statefulsets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps,resources=statefulsets/status,verbs=get
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps,resources=deployments/status,verbs=get
// +kubebuilder:rbac:groups=core,resources=configmaps;secrets;services;serviceaccounts,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=networking.k8s.io,resources=networkpolicies,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services/status,verbs=get
// +kubebuilder:rbac:groups=extensions,resources=ingresses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=extensions,resources=ingresses/status,verbs=get
// +kubebuilder:rbac:groups=core,resources=events,verbs=get;list;watch;create;update;patch;delete

// DeviceServerReconciler reconciles a DeviceServer object
type DeviceServerReconciler struct {
	client.Client
	Log       logr.Logger
	Scheme    *runtime.Scheme
	CustomLog dtypes.CustomLogger
	Recorder  record.EventRecorder
}

func (r *DeviceServerReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("deviceserver", req.NamespacedName)
	clog := r.CustomLog.WithValues("deviceserver", req.NamespacedName)
	_ = clog
	memcache := GetCache()
	shortSleep, _ := time.ParseDuration(fmt.Sprintf("%d", 20+rand.Intn(10)) + "s")  // about 20 seconds
	mediumSleep, _ := time.ParseDuration(fmt.Sprintf("%d", 60+rand.Intn(10)) + "s") // about 1 minute
	bigSleep, _ := time.ParseDuration(fmt.Sprintf("%d", 300+rand.Intn(30)) + "s")   // about 5 minutes

	Debugf(log, "Incoming request: %v", req)

	var deviceserver tangov2.DeviceServer
	if err := r.Get(ctx, req.NamespacedName, &deviceserver); err != nil {
		if client.IgnoreNotFound(err) == nil {

			Debugf(log, "unable to fetch DeviceServer(delete in progress?): "+err.Error())
			not_found_error.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
			reconcile_failures.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
			// we'll ignore not-found errors, since they can't be fixed by an immediate
			// requeue (we'll need to wait for a new notification), and we cannot get them
			// on deleted requests.
			return ctrl.Result{}, client.IgnoreNotFound(err)
		}
	}

	if deviceserver.Annotations == nil {
		deviceserver.Annotations = map[string]string{}
	}
	doneKey := fmt.Sprintf("%s.%s.done", req.Namespace, req.Name)
	reconcile_count.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
	currentDeviceServerStatefulSet, err := r.getStatefulSet(deviceserver.Namespace, "ds-"+deviceserver.Name, &deviceserver)
	if err == nil && currentDeviceServerStatefulSet.Status.Replicas == currentDeviceServerStatefulSet.Status.AvailableReplicas {
		reconcile_count_after_success.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
		cached_ds, done := memcache.Get(doneKey)
		if done {
			Debugf(log, "Done key set - Check for spec changes: %s (%d/%d)", deviceserver.Status.State, deviceserver.Status.Replicas, deviceserver.Status.Succeeded)
			if diff := deep.Equal(cached_ds, deviceserver.Spec); diff != nil {
				Debugf(log, "Specification changes: %+v", diff)
			} else {
				Debugf(log, "No difference, sleep")
				r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Warning", "DeviceServer already deployed - sleep: %q", deviceserver.Name)
				return ctrl.Result{}, nil
			}
		}
	}

	deviceserver.Status.Replicas = 0
	deviceserver.Status.Succeeded = 0
	deviceserver.Status.Resources = tangov2.Resources{}
	deviceserver.Status.State = "Building"
	var databaseds tangov2.DatabaseDS
	databasedsobjkey := client.ObjectKey{
		Namespace: req.Namespace,
		Name:      deviceserver.Spec.DatabaseDS,
	}

	// setup configuration.
	dcontext := dtypes.SetConfig(deviceserver)
	if dcontext.Image == "" {
		dcontext.Image = dtypes.Image
	}
	if dcontext.PullPolicy == "" {
		dcontext.PullPolicy = dtypes.PullPolicy
	}
	// check DatabaseDS resource unless direct connection configured
	// we'll ignore not-found errors, since they can't be fixed by an immediate
	// requeue (we'll need to wait for a new notification), and we can get them
	// on deleted requests.
	// this is a direct connection to a DatabaseDS resource, so set the hostname correctly
	// find the host and port
	// if parse failed then default to standard port 10000
	// if this is a DirectConnection (manually configured TANGO_HOST) then carry on
	// if this is not a DirectConnection and the DatabaseDS is not up correctly then delay until it is
	// we'll ignore not-found errors, since they can't be fixed by an immediate
	// requeue (we'll need to wait for a new notification), and we can get them
	// on deleted requests.
	// set the status to waiting
	// is it running and "ready"?

	returnValue, err := r.checkDatabaseDS(ctx, dcontext, databasedsobjkey, databaseds, deviceserver, req, memcache)
	if err != nil {
		Errorf(log, err, "Databaseds check failed: %s", err.Error())
	}
	if returnValue == 0 {
		Debugf(log, fmt.Sprintf("DatabaseDS IS ready: %s - %s", deviceserver.Spec.DatabaseDS, databaseds.Status.State))
		time_ready := metav1.Now().UTC()
		if dds_ready_at_str, ok := deviceserver.Annotations["metrics/dds-ready-at"]; !ok {
			deviceserver.Annotations["metrics/dds-ready-at"] = time_ready.Format(time.StampNano)
			r.setDSAnnotations(ctx, req, deviceserver.Annotations)
		} else {
			time_ready, _ = time.Parse(time.StampNano, dds_ready_at_str)
		}

		time_start := deviceserver.CreationTimestamp.Time
		duration := time_ready.Sub(time_start).Seconds()
		if duration > 0 {
			Debugf(log, "databaseds_wait_time start/ready: %s/%s %s.%s", time_start.Format(time.StampNano), time_ready.Format(time.StampNano), deviceserver.GetName(), deviceserver.Namespace)
			databaseds_wait_time.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Set(duration)
		}
	} else {
		deviceserver.Status.State = "Waiting(" + deviceserver.Spec.DatabaseDS + ")"
		r.Status().Update(ctx, &deviceserver)
		pending_databaseds.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
		reconcile_failures.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
		return ctrl.Result{RequeueAfter: r.GetWaitTime(deviceserver, rand.Intn(10))}, nil
	}

	// if there is a dependency list then iterate over them to make sure they are up first
	if deviceserver.Spec.DependsOn != nil {
		var dependsOn []string
		externalRetryKey := fmt.Sprintf("%s.%s.retry", req.Namespace, req.Name)

		// if we exceed the retry threshold then kill the hot loop on this and extend the wait
		strRetries, retry := memcache.Get(externalRetryKey)
		retries, err := strconv.Atoi(fmt.Sprintf("%s", strRetries))
		if err != nil {
			retries = 0
		}

		if retry {
			if retries > retriesBeforeSleep {
				Infof(log, "Dependency RETRY Cache call says SLEEP - exit: %s => %s/%s = [%v] %v", externalRetryKey, deviceserver.GetName(), deviceserver.Namespace, retry, strRetries)
				r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Warning", "Waiting for Dependencies - sleep -DeviceServer: %q", deviceserver.Name)
				// return ctrl.Result{RequeueAfter: r.GetWaitTime(deviceserver, rand.Intn(200))}, nil
				return ctrl.Result{RequeueAfter: shortSleep}, nil
			}
		}

		for _, dependency := range deviceserver.Spec.DependsOn {

			returnValue, err := r.checkDependency(ctx, req, dependency, dcontext, deviceserver, retries, memcache, externalRetryKey)
			if err != nil {
				Errorf(log, err, "Failed to check dependency: %s", dependency)
				return ctrl.Result{}, err
			}
			if returnValue == 2 {
				deviceserver.Status.State = "Waiting(" + dependency + ")"
				r.Status().Update(ctx, &deviceserver)
				pending_dependency.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace, dependency).Inc()
				reconcile_failures.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
				retries += 1
				return ctrl.Result{RequeueAfter: r.GetDependencyWaitTime(retries)}, nil
			}
			if returnValue == 3 {
				deviceserver.Status.State = "Waiting?"
				r.Status().Update(ctx, &deviceserver)
				pending_dependency.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace, dependency).Inc()
				reconcile_failures.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
				retries += 1
				return ctrl.Result{RequeueAfter: r.GetDependencyWaitTime(retries)}, nil
			}
		}
		// reassign with only the remote pings
		dcontext.DependsOn = dependsOn
	}

	time_ready := metav1.Now().UTC()
	if dependson_ready_at_str, ok := deviceserver.Annotations["metrics/dependson-ready-at"]; !ok {
		deviceserver.Annotations["metrics/dependson-ready-at"] = time_ready.Format(time.StampNano)
		r.setDSAnnotations(ctx, req, deviceserver.Annotations)
	} else {
		time_ready, _ = time.Parse(time.StampNano, dependson_ready_at_str)
	}

	time_start := time.Now().UTC()
	if dds_ready_at_str, ok := deviceserver.Annotations["metrics/dds-ready-at"]; ok {
		time_start, _ = time.Parse(time.StampNano, dds_ready_at_str)
	}
	duration := time_ready.Sub(time_start).Seconds()
	if duration > 0 {
		Debugf(log, "dependson_wait_time start/wait: %s/%s %s.%s", time_start.Format(time.StampNano), time_ready.Format(time.StampNano), deviceserver.GetName(), deviceserver.Namespace)
		dependson_wait_time.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Set(duration)
	}

	var childStatefulSets appsv1.StatefulSetList
	if err := r.List(ctx, &childStatefulSets, client.InNamespace(req.Namespace), client.MatchingFields{deviceServerOwnerKey: req.Name}); err != nil {
		log.Error(err, "unable to list child StatefulSets")
		if client.IgnoreNotFound(err) != nil {
			not_found_statefulset.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
			reconcile_failures.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
			return ctrl.Result{}, err
		}
	}

	var (
		childSS      int = 0
		childSSready int = 0
	)
	// make sure that the children exist and that the status is correct
	for _, sschld := range childStatefulSets.Items {
		childSS += 1
		if sschld.Status.ReadyReplicas > 0 {
			childSSready += 1
		}
	}

	Debugf(log, "incoming context: %+v", deviceserver)

	/*
		configureDevice returnValue:
		0 - OK
		1 - invalid configuration on device server specification
		2 - cached invalid configuration
		3 - json marshal error
		4 - http new request error
		5 - http post request error
		6 - dsconfig configuration invalid
		7 - dsconfig error
	*/
	returnValue, err = r.configureDevice(dcontext, deviceserver, req, memcache)
	if returnValue == 0 {
		config_applied.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
		Debugf(log, fmt.Sprintf("DeviceServer config applied - set cache: %s/%s", deviceserver.GetName(), deviceserver.Namespace))

		time_ready := metav1.Now().UTC()
		if setupdb_ready_at_str, ok := deviceserver.Annotations["metrics/setupdb-ready-at"]; !ok {
			deviceserver.Annotations["metrics/setupdb-ready-at"] = time_ready.Format(time.StampNano)
			r.setDSAnnotations(ctx, req, deviceserver.Annotations)
		} else {
			time_ready, _ = time.Parse(time.StampNano, setupdb_ready_at_str)
		}

		time_start := time.Now().UTC()
		if dependson_ready_at_str, ok := deviceserver.Annotations["metrics/dependson-ready-at"]; ok {
			time_start, _ = time.Parse(time.StampNano, dependson_ready_at_str)
		}
		duration := time_ready.Sub(time_start).Seconds()
		if duration > 0 {
			Debugf(log, "setupdb_wait_time start/ready: %s/%s %s.%s", time_start.Format(time.StampNano), time_ready.Format(time.StampNano), deviceserver.GetName(), deviceserver.Namespace)
			setupdb_wait_time.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Set(duration)
		}

		r.Recorder.Eventf(&deviceserver, corev1.EventTypeNormal, "Info", "DeviceServer config applied: %q", deviceserver.Name)
	} else {
		if returnValue == 6 || returnValue == 2 {
			deviceserver.Status.State = "Error(config)"
			r.Status().Update(ctx, &deviceserver)
			r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Failed", "DeviceServer config error: %q", err.Error())
			invalid_configuration.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
			reconcile_failures.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
			// medium hoping for a change in the device server definition
			return ctrl.Result{RequeueAfter: mediumSleep}, fmt.Errorf("configureDevice error: %s", err.Error())
		} else {
			// this means an general error in the dsconfig tool. We log the error because it should not happen
			Errorf(log, err, "configureDevice Error: %s", err.Error())
			deviceserver.Status.State = "Error(Json2Tango)"
			r.Status().Update(ctx, &deviceserver)
			r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Failed", "DeviceServer config error: %q", err.Error())
			config_job_failed.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
			reconcile_failures.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
			return ctrl.Result{RequeueAfter: shortSleep}, fmt.Errorf("configureDevice error: %s", err.Error())
		}
	}

	if deviceserver.Spec.Command == "" {
		// check Script - is it a script, file or URL
		scriptName, scriptContents, _, err := utils.CheckScript(dcontext.Script, dcontext.DSName)
		if err != nil {
			Errorf(log, err, "DeviceServerJob script is invalid: %s", err.Error())
			r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Failed", "DeviceServerJob script is invalid: %q", deviceserver.Name)
			script_setup_failed.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
			reconcile_failures.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
			return ctrl.Result{RequeueAfter: bigSleep}, nil
		}
		dcontext.ScriptContents = scriptContents
		if len(scriptContents) > 0 {
			dcontext.AppName = scriptName
		}
	}

	// Get resource details
	resources, err := r.resourceDetails(dcontext)
	if err != nil {
		deviceserver.Status.Resources = resources
		// no metrics here since it's unclear why this should happen!
		return ctrl.Result{}, err
	}
	deviceserver.Status.Resources = resources

	// Generate desired children.

	// process the ConfigMap
	if res, err := r.checkSetConfigMap(ctx, req, &dcontext, &deviceserver); err != nil {
		if err = r.Status().Update(ctx, &deviceserver); err != nil {
			Errorf(log, err, "unable to update DeviceServer status: %s", req.Name)
		}
		// no metrics here since it's unclear why this should happen!
		return res, err
	}

	dcontext.Metadata = deviceserver.ObjectMeta
	// val, _ := json.MarshalIndent(deviceserver.ObjectMeta, "", "")
	// Infof(log, "ObjectMeta: %s", val)

	// process the DeviceServer StatefulSet
	if res, err := r.checkSetDeviceServer(ctx, req, &dcontext, &deviceserver); err != nil {
		if err = r.Status().Update(ctx, &deviceserver); err != nil {
			Errorf(log, err, "unable to update DeviceServer status: %s", req.Name)
		}
		// no metrics here since it's unclear why this should happen!
		return res, err
	}
	// Compute status based on latest observed state. Check the running child StatefulSet
	// deviceserver.Status.Replicas is automatically incremented when each of the StatefulSet are Ready
	// (don't know how this magic happens???)
	// check:
	// currentDeviceServerStatefulSet.Status.Replicas != currentDeviceServerStatefulSet.Status.AvailableReplicas
	// tells whether Pods are broken/not launched correctly/yet
	if len(dcontext.ExternalDeviceServer) > 0 ||
		(deviceserver.Status.Replicas == 1 &&
			deviceserver.Status.Replicas == deviceserver.Status.Succeeded &&
			childSS == 1 && childSSready == childSS) {
		deviceserver.Status.State = "Running"
		memcache.Set(doneKey, deviceserver.Spec, cache.DefaultExpiration)
		time_ready := metav1.Now().UTC()
		if ds_ready_at_str, ok := deviceserver.Annotations["metrics/ds-ready-at"]; !ok {
			deviceserver.Annotations["metrics/ds-ready-at"] = time_ready.Format(time.StampNano)
			r.setDSAnnotations(ctx, req, deviceserver.Annotations)
		} else {
			time_ready, _ = time.Parse(time.StampNano, ds_ready_at_str)
		}

		time_start := deviceserver.CreationTimestamp.Time
		duration := time_ready.Sub(time_start).Seconds()
		Debugf(log, "startup_time start/ready: %s/%s %s.%s", time_start.Format(time.StampNano), time_ready.Format(time.StampNano), deviceserver.GetName(), deviceserver.Namespace)
		if duration > 0 {
			startup_time.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Set(duration)
		}
		r.Recorder.Eventf(&deviceserver, corev1.EventTypeNormal, "Success", "DeviceServer deployed: %q", deviceserver.Name)

		if jsonDSConfigStr, err := r.getConfigFromDSConfig(dtypes.PingSvcUrl, dcontext.DatabaseDS, dcontext.Port, deviceserver.Spec.DSName, deviceserver.Spec.Args); err != nil {
			Errorf(log, err, "unable to get json config: %s", req.Name)
		} else {
			deviceserver.Annotations["json-dsconfig-ret-time"] = metav1.Now().UTC().Format(time.StampNano)
			deviceserver.Annotations["json-dsconfig"] = jsonDSConfigStr
			r.setDSAnnotations(ctx, req, deviceserver.Annotations)
		}
	}

	r.setMetricsPullingTime(deviceserver, req, log)

	// set the status and go home
	// no metrics here since it's unclear why this should happen!

	if jsonDevStateStr, err := r.getDeviceStates(dtypes.PingSvcUrl, dcontext.DatabaseDS, dcontext.Port, deviceserver.Spec.DSName, deviceserver.Spec.Args); err != nil {
		Errorf(log, err, "unable to get devices state: %s", req.Name)
	} else {

		var jsonDsConfigMap map[string]any
		err = json.Unmarshal([]byte(jsonDevStateStr), &jsonDsConfigMap)
		if err != nil {
			Errorf(log, err, "unable to unmarshal devices state: %s", req.Name)
		}
		deviceserver.Status.DeviceCount = int32(jsonDsConfigMap["dev_count"].(float64))
		deviceserver.Status.DeviceReadyCount = int32(jsonDsConfigMap["dev_ready_count"].(float64))
		var devicesList []tangov2.Device
		devices := jsonDsConfigMap["devices"].([]any)
		for index := range devices {
			jsonDev := devices[index].(map[string]any)
			rettime_str := jsonDev["rettime"].(string)
			layout := "2006-01-02T15:04:05.000000Z"
			rettime0, err := time.Parse(layout, rettime_str)
			if err != nil {
				rettime0 = time.Now().UTC()
			}
			device := &tangov2.Device{
				Name:    jsonDev["name"].(string),
				Ping:    int32(jsonDev["ping"].(float64)),
				State:   jsonDev["state"].(string),
				Status:  jsonDev["status"].(string),
				Class:   jsonDev["class"].(string),
				RetTime: metav1.NewTime(rettime0),
			}
			devicesList = append(devicesList, *device)
		}
		deviceserver.Status.Devices = devicesList
	}

	if _, err := r.setStatus(ctx, req, deviceserver.Status); err != nil {
		Errorf(log, err, "unable to update DeviceServer status: %s", req.Name)
		// no metrics here since it's unclear why this should happen!
		return ctrl.Result{}, err
	}

	Infof(log, "Reconcile Complete: %s (%d/%d)", deviceserver.Status.State, deviceserver.Status.Replicas, deviceserver.Status.Succeeded)
	return ctrl.Result{}, nil
}

func (r *DeviceServerReconciler) setStatus(ctx context.Context, req reconcile.Request, state2update tangov2.DeviceServerStatus) (bool, error) {
	log := r.Log.WithValues("deviceserver", req.NamespacedName)
	var deviceserver tangov2.DeviceServer
	if err := r.Get(ctx, req.NamespacedName, &deviceserver); err != nil {
		Errorf(log, err, "unable to get DeviceServer: %s", req.Name)
		return false, err
	}
	deviceserver.Status = state2update
	if err := r.Status().Update(ctx, &deviceserver); err != nil {
		Errorf(log, err, "unable to update DeviceServer status: %s", req.Name)
		return false, err
	}
	return true, nil
}

func (r *DeviceServerReconciler) setDSAnnotations(ctx context.Context, req reconcile.Request, annotations map[string]string) (bool, error) {
	log := r.Log.WithValues("deviceserver", req.NamespacedName)
	var deviceserver tangov2.DeviceServer
	if err := r.Get(ctx, req.NamespacedName, &deviceserver); err != nil {
		Errorf(log, err, "unable to get DeviceServer: %s", req.Name)
		return false, err
	}
	deviceserver.ObjectMeta.Annotations = annotations
	if err := r.Update(ctx, &deviceserver); err != nil {
		Errorf(log, err, "unable to update DeviceServer annotations: %s", req.Name)
		return false, err
	}
	return true, nil
}

func (r *DeviceServerReconciler) SetupWithManager(mgr ctrl.Manager) error {

	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &appsv1.StatefulSet{}, deviceServerOwnerKey, func(rawObj client.Object) []string {
		// grab the StatefulSet object, extract the owner...
		deployment := rawObj.(*appsv1.StatefulSet)
		owner := metav1.GetControllerOf(deployment)
		if owner == nil {
			return nil
		}
		// ...make sure it's a DeviceServer ...
		if !IsValidVersion(owner.APIVersion) || owner.Kind != "DeviceServer" {
			return nil
		}

		// ...and if so, return it
		return []string{owner.Name}
	}); err != nil {
		return err
	}

	log := r.Log.WithValues("deviceserver", "DeleteEvent")
	var noLeader bool = false
	return ctrl.NewControllerManagedBy(mgr).
		For(&tangov2.DeviceServer{}).
		Owns(&appsv1.StatefulSet{}).
		Owns(&corev1.Service{}).
		Owns(&corev1.ServiceAccount{}).
		Owns(&corev1.ConfigMap{}).
		// Owns(&networkingv1.NetworkPolicy{}).
		WithEventFilter(predicate.Funcs{
			DeleteFunc: func(e event.DeleteEvent) bool {
				// The reconciler adds a finalizer so we perform clean-up
				// when the delete timestamp is added
				// add your code for deleting zombie ServiceAccounts here
				// if reflect.TypeOf(e.Object).String() == "*v1.ServiceAccount" || reflect.TypeOf(e.Object).String() == "*v1.DeviceServer" {
				Debugf(log, "****** DeleteEvent: %s/%s/%s", reflect.TypeOf(e.Object).String(), e.Object.GetNamespace(), e.Object.GetName())
				// }

				// Suppress Delete events to avoid filtering them out in the Reconcile function
				return false
			},
		}).
		WithOptions(controller.Options{
			MaxConcurrentReconciles: dtypes.MaxConcurrentReconciles,
			NeedLeaderElection:      &noLeader,
		}).
		Complete(r)
}

func (r *DeviceServerReconciler) RawConnect(host string, port string) bool {
	// Check a remote host and port
	timeout := time.Second
	conn, err := net.DialTimeout("tcp", net.JoinHostPort(host, port), timeout)
	if err != nil {
		Debugf(r.Log, "Connecting error: %s", err)
		return false
	}
	if conn != nil {
		defer conn.Close()
		Debugf(r.Log, "Opened: %s", net.JoinHostPort(host, port))
		return true
	}
	return false
}

func (r *DeviceServerReconciler) GetWaitTime(deviceserver tangov2.DeviceServer, randomInt int) time.Duration {
	// wait proportionally to the dependency length
	milleseconds_delay := len(deviceserver.Spec.DependsOn) + randomInt
	return time.Millisecond * time.Duration(minWaitMilliSecs+(milleseconds_delay*100))

}

func (r *DeviceServerReconciler) GetDependencyWaitTime(iteration int) time.Duration {
	// wait proportionally to the dependency length
	milleseconds_delay := (minWaitMilliSecs * iteration) + rand.Intn(100*iteration)
	return time.Millisecond * time.Duration(milleseconds_delay)

}

func init() {
	// Register custom metrics with the global prometheus registry
	metrics.Registry.MustRegister(not_found_error, not_found_databaseds, not_found_statefulset, invalid_configuration, config_job_failed, tempfile_allocate_problem, tempfile_write_problem, tempfile_close_problem, pending_databaseds, pending_dependency, config_applied, reconcile_count, reconcile_count_after_success, startup_time, image_pulling, databaseds_wait_time, dependson_wait_time, setupdb_wait_time)
}
