/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package controllers

import (
	"crypto/sha256"
	"fmt"
	"reflect"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/ska-telescope/ska-tango-operator/models"

	"context"

	ctrl "sigs.k8s.io/controller-runtime"

	dtypes "gitlab.com/ska-telescope/ska-tango-operator/types"

	tangov2 "gitlab.com/ska-telescope/ska-tango-operator/api/v2"
)

// Check and set the ConfigMap for DeviceServer
func (r *DeviceServerReconciler) checkSetConfigMap(ctx context.Context, req ctrl.Request, dcontext *dtypes.OperatorContext, deviceserver *tangov2.DeviceServer) (ctrl.Result, error) {
	var currentConfig *corev1.ConfigMap

	log := r.Log.WithValues("deviceserver", req.NamespacedName)

	currentConfig, _ = r.getConfig(deviceserver.Namespace, "ds-configs-"+deviceserver.Name)

	// create ConfigMap
	configMap, err := models.DeviceServerConfigs(*dcontext)
	if err != nil {
		Errorf(log, err, "DeviceServerConfigs Error: %+v\n", err)
		deviceserver.Status.State = fmt.Sprintf("DeviceServerConfigs Error: %+v\n", err)
		return ctrl.Result{}, err
	}
	configMap.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(deviceserver, tangov2.GroupVersion.WithKind("DeviceServer"))}

	// set the reference
	if err := ctrl.SetControllerReference(deviceserver, configMap, r.Scheme); err != nil {
		Errorf(log, err, "DeviceServerConfigs Error: %+v\n", err)
		return ctrl.Result{}, err
	}

	Debugf(log, "DeviceServerConfigs: %+v", *configMap)
	dcontext.ConfigCheckSum = fmt.Sprintf("%x", sha256.Sum256([]byte(fmt.Sprintf("%+v", configMap.Data))))

	// create dependent ConfigMap
	if currentConfig == nil {
		Debugf(log, "###### Create ConfigMap #######")

		// // create cluster wide DNS NetworkPolicy
		// if !dcontext.DisablePolicies {
		// 	dnsNetworkPolicy, err := models.DNSNetworkPolicy(*dcontext)
		// 	if err != nil {
		// 		Errorf(log, err, "DNSNetworkPolicy Error: %+v\n", err)
		// 		deviceserver.Status.State = fmt.Sprintf("DNSNetworkPolicy Error: %+v\n", err)
		// 		return ctrl.Result{}, err
		// 	}
		// 	dnsNetworkPolicy.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(deviceserver, tangov2.GroupVersion.WithKind("DeviceServer"))}
		// 	Debugf(log, "DNSNetworkPolicy: %+v", *dnsNetworkPolicy)
		// 	// set the reference
		// 	if err := ctrl.SetControllerReference(deviceserver, dnsNetworkPolicy, r.Scheme); err != nil {
		// 		Errorf(log, err, "DNSNetworkPolicy Error: %+v\n", err)
		// 		return ctrl.Result{}, err
		// 	}
		// 	// ...and create it on the cluster
		// 	if err := r.Create(ctx, dnsNetworkPolicy); err != nil {
		// 		log.Error(err, "unable to create DNSNetworkPolicy for DeviceServer", "NetworkPolicy", dnsNetworkPolicy)
		// 		return ctrl.Result{}, err
		// 	}
		// }

		// ...and create it on the cluster
		if err := r.Create(ctx, configMap); err != nil {
			log.Error(err, "unable to create ConfigMap for DeviceServer", "configMap", configMap)
			return ctrl.Result{}, err
		}
	} else {
		// Do we need to Update the ConfigMap ?
		if !reflect.DeepEqual(configMap.Data, currentConfig.Data) {
			err = r.Update(ctx, configMap)
			if err != nil {
				log.Error(err, "unable to update ConfigMap for DeviceServer", "configMap", configMap)
				deviceserver.Status.State = fmt.Sprintf("unable to update ConfigMap Error: %+v\n", err)
				return ctrl.Result{}, err
			}
		}

	}

	return ctrl.Result{}, nil
}

// Check and set the StatefulSet for the DeviceServer
func (r *DeviceServerReconciler) checkSetDeviceServer(ctx context.Context, req ctrl.Request, dcontext *dtypes.OperatorContext, deviceserver *tangov2.DeviceServer) (ctrl.Result, error) {

	var currentDeviceServerStatefulSet *appsv1.StatefulSet
	var currentDeviceServerService *corev1.Service
	// var currentDeviceServerServiceAccount *corev1.ServiceAccount

	log := r.Log.WithValues("deviceserver", req.NamespacedName)

	currentDeviceServerStatefulSet, _ = r.getStatefulSet(deviceserver.Namespace, "ds-"+deviceserver.Name, deviceserver)

	// set the default tango host
	if !dcontext.DirectConnection {
		dcontext.DatabaseDS = fmt.Sprintf("%s.%s.svc.%s", dcontext.DatabaseDS, req.Namespace, dcontext.ClusterDomain)
	}
	deviceServerStatefulSet, err := models.DeviceServerStatefulSet(*dcontext)
	if err != nil {
		Errorf(log, err, "DeviceServerStatefulSet Error: %+v\n", err)
		deviceserver.Status.State = fmt.Sprintf("DeviceServerStatefulSet Error: %+v\n", err)
		return ctrl.Result{}, err
	}
	deviceServerStatefulSet.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(deviceserver, tangov2.GroupVersion.WithKind("DeviceServer"))}
	Debugf(log, "DeviceServerStatefulSet: %+v", *deviceServerStatefulSet)
	// set the reference
	if err := ctrl.SetControllerReference(deviceserver, deviceServerStatefulSet, r.Scheme); err != nil {
		Errorf(log, err, "DeviceServerStatefulSet Error: %+v\n", err)
		return ctrl.Result{}, err
	}

	// look for the Service
	currentDeviceServerService, _ = r.getService(deviceserver.Namespace, "ds-"+deviceserver.Name)
	Debugf(log, "###### Create Device Server - Service #######")
	if currentDeviceServerService == nil {

		// create Service
		// is this an external pointer only
		if len(dcontext.ExternalDeviceServer) > 0 {
			dcontext.ServiceType = "ExternalName"
			// fudge the numbers as we aren't going to have a StatefulSet
			deviceserver.Status.Replicas++
			deviceserver.Status.Succeeded++

			// is the a load balancer type
		} else if dcontext.EnableLoadBalancer {
			dcontext.ServiceType = "LoadBalancer"
		}

		deviceServerService, err := models.DeviceServerService(*dcontext)
		if err != nil {
			Errorf(log, err, "DeviceServerService Error: %+v\n", err)
			deviceserver.Status.State = fmt.Sprintf("DeviceServerService Error: %+v\n", err)
			return ctrl.Result{}, err
		}
		deviceServerService.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(deviceserver, tangov2.GroupVersion.WithKind("DeviceServer"))}
		Debugf(log, "DeviceServerService: %+v", *deviceServerService)
		// set the reference
		if err := ctrl.SetControllerReference(deviceserver, deviceServerService, r.Scheme); err != nil {
			Errorf(log, err, "DeviceServerService Error: %+v\n", err)
			return ctrl.Result{}, err
		}
		// ...and create it on the cluster
		if err := r.Create(ctx, deviceServerService); err != nil {
			log.Error(err, "unable to create Service for DeviceServer", "Service", deviceServerService)
			return ctrl.Result{}, err
		}
	}

	// create device server statefulset - but only if this is not an external
	if len(dcontext.ExternalDeviceServer) == 0 {
		Debugf(log, "###### Create Device Server - StatefulSet #######")
		if currentDeviceServerStatefulSet == nil {

			// create StatefulSet
			// ...and create it on the cluster
			if err := r.Create(ctx, deviceServerStatefulSet); err != nil {
				log.Error(err, "unable to create StatefulSet for DeviceServer", "StatefulSet", deviceServerStatefulSet)
				return ctrl.Result{}, err
			}
			r.Recorder.Eventf(deviceserver, corev1.EventTypeNormal, "Created", "Created  deployment %q", deviceServerStatefulSet.Name)

		} else {

			/***********************************************************************************
			PXH - disable until calico issue is resolved: https://github.com/kubernetes/kubernetes/issues/116632
			and https://github.com/projectcalico/calico/issues/5715
			// look for the ServiceAccount
			currentDeviceServerServiceAccount, _ = r.getServiceAccount(deviceserver.Namespace, "ds-serviceaccount-"+deviceserver.Name)
			Debugf(log, "###### Create Device Server - ServiceAccount #######")
			if currentDeviceServerServiceAccount == nil {

				// create ServiceAccount
				serviceAccount, err := models.DeviceServerServiceAccount(*dcontext)
				if err != nil {
					Errorf(log, err, "DeviceServerServiceAccount Error: %+v\n", err)
					deviceserver.Status.State = fmt.Sprintf("DeviceServerServiceAccount Error: %+v\n", err)
					return ctrl.Result{}, err
				}

				serviceAccount.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(deviceserver, tangov2.GroupVersion.WithKind("DeviceServer"))}

				// serviceAccount.ObjectMeta.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(currentDeviceServerStatefulSet, appsv1.SchemeGroupVersion.WithKind("StatefulSet"))}
				Debugf(log, "DeviceServerServiceAccount: %+v", *serviceAccount)
				// set the reference
				// if err := ctrl.SetControllerReference(currentDeviceServerStatefulSet, serviceAccount, r.Scheme); err != nil {
				if err := ctrl.SetControllerReference(deviceserver, serviceAccount, r.Scheme); err != nil {
					Errorf(log, err, "DeviceServerServiceAccount Error: %+v\n", err)
					return ctrl.Result{}, err
				}

				// // ...and create the ServiceAccount on the cluster
				if err := r.Create(ctx, serviceAccount); err != nil {
					log.Error(err, "unable to create DeviceServerServiceAccount for DeviceServer", "ServiceAccount", serviceAccount)
					return ctrl.Result{}, err
				}
			}
			**********************************************************************************/

			// Do we need to Update the StatefulSet based on the ConfigMap signature change or has the containers signature changed ?
			if !reflect.DeepEqual(currentDeviceServerStatefulSet.ObjectMeta.Annotations, deviceServerStatefulSet.ObjectMeta.Annotations) ||
				!reflect.DeepEqual(currentDeviceServerStatefulSet.Spec.Template.Spec.Containers, deviceServerStatefulSet.Spec.Template.Spec.Containers) {
				err = r.Update(ctx, deviceServerStatefulSet)
				if err != nil {
					log.Error(err, "unable to update StatefulSet for DeviceServer", "deviceServerStatefulSet", deviceServerStatefulSet)
					deviceserver.Status.State = fmt.Sprintf("unable to update StatefulSet Error: %+v\n", err)
					return ctrl.Result{}, err
				}
			}

		}
	}

	return ctrl.Result{}, nil
}
