/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package controllers

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/patrickmn/go-cache"

	"github.com/go-logr/logr"
	dtypes "gitlab.com/ska-telescope/ska-tango-operator/types"
	"gitlab.com/ska-telescope/ska-tango-operator/utils"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	tangov1 "gitlab.com/ska-telescope/ska-tango-operator/api/v1"
	tangov2 "gitlab.com/ska-telescope/ska-tango-operator/api/v2"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var memCache *cache.Cache

func IsValidVersion(version string) bool {
	switch version {
	case
		tangov1.GroupVersion.String(),
		tangov2.GroupVersion.String():
		return true
	}
	return false
}

// create and return the in memory cache
func GetCache() *cache.Cache {
	if memCache == nil {
		memCache = cache.New(5*time.Minute, 10*time.Minute)
	}
	return memCache
}

// Errorf helper
func Errorf(log logr.Logger, err error, format string, a ...interface{}) {
	log.Error(err, fmt.Sprintf(format, a...))
}

// Infof helper
func Infof(log logr.Logger, format string, a ...interface{}) {
	log.Info(fmt.Sprintf(format, a...))
}

// Debugf helper
func Debugf(log logr.Logger, format string, a ...interface{}) {
	log.V(1).Info(fmt.Sprintf(format, a...))
}

// set ownership and reference
func setOwnerReferences(res metav1.Object, cntlr metav1.Object, parentKind string, s *runtime.Scheme, t metav1.TypeMeta, log logr.Logger) error {

	// set ownership
	res.SetOwnerReferences([]metav1.OwnerReference{*metav1.NewControllerRef(cntlr, tangov2.GroupVersion.WithKind(parentKind))})

	Debugf(log, "%s: %+v\n", t.GetObjectKind(), res)
	// set the reference
	if err := ctrl.SetControllerReference(cntlr, res, s); err != nil {
		Errorf(log, err, "%s Error: %+v\n", t.GetObjectKind(), err)
		return err
	}
	return nil
}

// read back the status info for the Service resource
func (r *DeviceServerReconciler) serviceStatus(dcontext dtypes.OperatorContext, name string) (tangov2.Resources, error) {
	ctx := context.Background()

	objkey := client.ObjectKey{
		Namespace: dcontext.Namespace,
		Name:      name,
	}

	log := r.Log.WithValues("service", objkey)

	service := corev1.Service{}
	if err := r.Get(ctx, objkey, &service); err != nil {
		Debugf(log, "serviceStatus.Get Error: %+v\n", err.Error())
		return tangov2.Resources{}, client.IgnoreNotFound(err)
	}
	owner := metav1.GetControllerOf(&service)
	if owner == nil {
		err := errors.New("serviceStatus.Get Error: owner empty")
		log.Error(err, "serviceStatus.Get Error: owner empty", owner)
		return tangov2.Resources{}, err
	}
	// ...make sure it's a DeviceServer...
	if !IsValidVersion(owner.APIVersion) || owner.Kind != "DeviceServer" {
		err := errors.New("serviceStatus.Get Error: wrong kind/owner")
		log.Error(err, "serviceStatus.Get Error: wrong kind/owner", owner)
		return tangov2.Resources{}, err
	}

	var ports []tangov2.Ports
	for _, p := range service.Spec.Ports {
		ports = append(ports, tangov2.Ports{Name: p.Name, Port: p.Port})
	}
	//portList := fmt.Sprintf("%s := %s/%s, %s", service.Name, service.Spec.Type, service.Spec.ClusterIP, strings.Join(ports[:], ","))
	var lbs []tangov2.LB
	if len(service.Status.LoadBalancer.Ingress) > 0 {
		for _, ing := range service.Status.LoadBalancer.Ingress {
			lbs = append(lbs, tangov2.LB{IP: ing.IP})
		}
	}
	//Debugf(log, "The service status: %s\n", portList)
	return tangov2.Resources{
		ServiceName: service.Name,
		ServiceType: string(service.Spec.Type),
		ClusterIP:   service.Spec.ClusterIP,
		Ports:       ports,
		Lbs:         lbs,
	}, nil
}

// pull together the resource details
func (r *DeviceServerReconciler) resourceDetails(dcontext dtypes.OperatorContext) (tangov2.Resources, error) {

	log := r.Log.WithName("resourceDetails")

	deviceServerService, err := r.serviceStatus(dcontext, "ds-"+dcontext.Name)
	if err != nil {
		Errorf(log, err, "serviceStatus DeviceServer Error: %+v\n", err)
		return tangov2.Resources{}, err
	}
	return deviceServerService, nil
}

// look up one of the StatefulSet
func (r *DeviceServerReconciler) getStatefulSet(namespace string, name string, deviceserver *tangov2.DeviceServer) (*appsv1.StatefulSet, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for statefulset", name)
	statefulsetKey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}
	statefulset := appsv1.StatefulSet{}
	if err := r.Get(ctx, statefulsetKey, &statefulset); err != nil {
		Debugf(log, "statefulset.Get Error: %+v\n", err.Error())
		return nil, err
	}
	deviceserver.Status.Replicas++
	if statefulset.Status.ReadyReplicas == statefulset.Status.Replicas {
		deviceserver.Status.Succeeded++
	}
	return &statefulset, nil
}

// look up one of the Service
func (r *DeviceServerReconciler) getService(namespace string, name string) (*corev1.Service, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for statefulset", name)

	// look for the Service
	objkey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}
	service := corev1.Service{}
	if err := r.Get(ctx, objkey, &service); err != nil {
		Debugf(log, "service.Get Error: %+v\n", err.Error())
		//Infof(log, "service.Get not found: %+v\n", objkey)
		return nil, err
	}
	return &service, nil
}

// look up one of the s
func (r *DeviceServerReconciler) getServiceAccount(namespace string, name string) (*corev1.ServiceAccount, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for statefulset", name)

	// look for the ServiceAccount
	objkey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}
	serviceAccount := corev1.ServiceAccount{}
	if err := r.Get(ctx, objkey, &serviceAccount); err != nil {
		Debugf(log, "serviceAccount.Get Error: %+v\n", err.Error())
		//Infof(log, "serviceAccount.Get not found: %+v\n", objkey)
		return nil, err
	}
	return &serviceAccount, nil
}

// read back the status info for the Config resource
func (r *DeviceServerReconciler) getConfig(namespace string, name string) (*corev1.ConfigMap, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for config", name)
	objkey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}

	config := corev1.ConfigMap{}
	if err := r.Get(ctx, objkey, &config); err != nil {
		Debugf(log, "configStatus.Get Error: %+v\n", err.Error())
		//Infof(log, "configStatus.Get not found: %+v\n", objkey)
		return nil, err
	}

	return &config, nil
}

// look up one of the StatefulSet
func (r *DatabaseDSReconciler) getStatefulSet(namespace string, name string, databaseds *tangov2.DatabaseDS) (*appsv1.StatefulSet, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for statefulset", name)
	statefulsetKey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}
	statefulset := appsv1.StatefulSet{}
	if err := r.Get(ctx, statefulsetKey, &statefulset); err != nil {
		Debugf(log, "statefulset.Get Error: %+v\n", err.Error())
		return nil, err
	}
	databaseds.Status.Replicas++
	if statefulset.Status.ReadyReplicas == statefulset.Status.Replicas {
		databaseds.Status.Succeeded++
	}
	return &statefulset, nil
}

// read back the status info for the Config resource
func (r *DatabaseDSReconciler) getConfig(namespace string, name string) (*corev1.ConfigMap, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for config", name)
	objkey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}

	config := corev1.ConfigMap{}
	if err := r.Get(ctx, objkey, &config); err != nil {
		Debugf(log, "configStatus.Get Error: %+v\n", err)
		return nil, err
	}

	return &config, nil
}

// read back the status info for the PVC resource
func (r *DatabaseDSReconciler) getPVC(namespace string, name string) (*corev1.PersistentVolumeClaim, error) {
	ctx := context.Background()
	log := r.Log.WithValues("looking for PVC", name)
	objkey := client.ObjectKey{
		Namespace: namespace,
		Name:      name,
	}

	pvc := corev1.PersistentVolumeClaim{}
	if err := r.Get(ctx, objkey, &pvc); err != nil {
		Debugf(log, "pvcStatus.Get Error: %+v\n", err)
		return nil, err
	}

	return &pvc, nil
}

// read back the status info for the Service resource
func (r *DatabaseDSReconciler) serviceStatus(dcontext dtypes.OperatorContext, name string) (tangov2.Resources, error) {
	ctx := context.Background()

	objkey := client.ObjectKey{
		Namespace: dcontext.Namespace,
		Name:      name,
	}

	log := r.Log.WithValues("service", objkey)

	service := corev1.Service{}
	if err := r.Get(ctx, objkey, &service); err != nil {
		Debugf(log, "serviceStatus.Get Error: %+v\n", err.Error())
		//Infof(log, "serviceStatus.Get not found: %+v\n", objkey)
		return tangov2.Resources{}, client.IgnoreNotFound(err)
	}
	owner := metav1.GetControllerOf(&service)
	if owner == nil {
		err := errors.New("serviceStatus.Get Error: owner empty")
		log.Error(err, "serviceStatus.Get Error: owner empty", owner)
		return tangov2.Resources{}, err
	}
	// ...make sure it's a DatabaseDS...
	if !IsValidVersion(owner.APIVersion) || owner.Kind != "DatabaseDS" {
		err := errors.New("serviceStatus.Get Error: wrong kind/owner")
		log.Error(err, "serviceStatus.Get Error: wrong kind/owner", owner)
		return tangov2.Resources{}, err
	}

	var ports []tangov2.Ports
	for _, p := range service.Spec.Ports {
		ports = append(ports, tangov2.Ports{Name: p.Name, Port: p.Port})
	}

	var lbs []tangov2.LB
	if len(service.Status.LoadBalancer.Ingress) > 0 {
		for _, ing := range service.Status.LoadBalancer.Ingress {
			lbs = append(lbs, tangov2.LB{IP: ing.IP})
		}
	}

	return tangov2.Resources{
		ServiceName: service.Name,
		ServiceType: string(service.Spec.Type),
		ClusterIP:   service.Spec.ClusterIP,
		Ports:       ports,
		Lbs:         lbs,
	}, nil
}

// read back the status info for the StatefulSet resource
func (r *DatabaseDSReconciler) statefulsetStatus(dcontext dtypes.OperatorContext, name string) (string, error) {

	ctx := context.Background()

	objkey := client.ObjectKey{
		Namespace: dcontext.Namespace,
		Name:      name,
	}

	log := r.Log.WithValues("statefulset", objkey)

	statefulset := appsv1.StatefulSet{}
	if err := r.Get(ctx, objkey, &statefulset); err != nil {
		Debugf(log, "statefulsetStatus.Get Error: %+v\n", err.Error())
		Infof(log, "statefulsetStatus.Get not found: %+v\n", objkey)
		return "NotFound", client.IgnoreNotFound(err)
	}
	owner := metav1.GetControllerOf(&statefulset)
	if owner == nil {
		err := errors.New("statefulsetStatus.Get Error: owner empty")
		log.Error(err, "statefulsetStatus.Get Error: owner empty", owner)
		return "OwnerError", err
	}
	// ...make sure it's a DatabaseDS...
	if !IsValidVersion(owner.APIVersion) || owner.Kind != "DatabaseDS" {
		err := errors.New("statefulsetStatus.Get Error: wrong kind/owner")
		log.Error(err, "statefulsetStatus.Get Error: wrong kind/owner", owner)
		return "OwnerKindError", err
	}

	Debugf(log, "The statefulset: %+v\n", statefulset)
	return fmt.Sprintf("%d of %d", statefulset.Status.ReadyReplicas, statefulset.Status.Replicas), nil
}

// pull together the resource details
func (r *DatabaseDSReconciler) resourceDetails(dcontext dtypes.OperatorContext) (tangov2.Resources, tangov2.Resources, string, string, error) {

	resDB, _ := r.statefulsetStatus(dcontext, "databaseds-tangodb-"+dcontext.Name)
	resDS, _ := r.statefulsetStatus(dcontext, "databaseds-ds-"+dcontext.Name)
	resTangoDBService, _ := r.serviceStatus(dcontext, "databaseds-tangodb-"+dcontext.Name)
	resDSService, _ := r.serviceStatus(dcontext, dcontext.Name)
	return resTangoDBService, resDSService, resDB, resDS, nil
}

// read back the events for the Pod resource
func (r *DeviceServerReconciler) getEvents(namespace string, name string, kind string) (*corev1.EventList, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	events, err := clientset.CoreV1().Events(namespace).List(context.TODO(), metav1.ListOptions{FieldSelector: "involvedObject.name=" + name, TypeMeta: metav1.TypeMeta{Kind: kind}})
	if err != nil {
		return nil, err
	}
	return events, nil
}

func (r *DeviceServerReconciler) isDatabaseDSReady(pingUrl string, tango_host string, port int32) (*http.Response, error) {
	log := r.Log.WithValues("isDatabaseDSReady", tango_host)
	url := fmt.Sprintf("https://%s/isdatabasedsready/%s/%d", pingUrl, tango_host, port)
	Debugf(log, "HTTP Is DatabaseDS Ready Url: %s", url)
	httpPingResponse, err := http.Get(url)
	if err != nil {
		Errorf(log, err, fmt.Sprintf("HTTP Is DatabaseDS Ready (%s:%d) Error: %s", tango_host, port, err))
		return httpPingResponse, err
	}
	body, err := io.ReadAll(httpPingResponse.Body)
	httpPingResponse.Body.Close()
	if httpPingResponse.StatusCode != 200 {
		Debugf(log, "HTTP Is DatabaseDS Ready: Code: %d, Body: %s", httpPingResponse.StatusCode, body)
		return httpPingResponse, nil
	}
	if err != nil {
		Errorf(log, err, fmt.Sprintf("HTTP Is DatabaseDS Ready (%s:%d) Error: %s", tango_host, port, err))
		return httpPingResponse, err
	}
	return httpPingResponse, nil
}

func (r *DeviceServerReconciler) pingDevice(pingUrl string, fullDeviceName string) (*http.Response, error) {
	log := r.Log.WithValues("pingDevice", fullDeviceName)
	url := fmt.Sprintf("https://%s/ping/%s", pingUrl, fullDeviceName)
	Debugf(log, "HTTP Ping Dev Url: %s", url)
	httpPingResponse, err := http.Get(url)
	if err != nil {
		Errorf(log, err, fmt.Sprintf("HTTP Ping Dev (%s) Error: %s", fullDeviceName, err))
		return httpPingResponse, err
	}
	body, err := io.ReadAll(httpPingResponse.Body)
	httpPingResponse.Body.Close()
	if httpPingResponse.StatusCode != 200 {
		Debugf(log, "HTTP Ping Dev Failed: Code: %d, Body: %s", httpPingResponse.StatusCode, body)
		return httpPingResponse, nil
	}
	if err != nil {
		Errorf(log, err, fmt.Sprintf("HTTP Ping Dev (%s) Error: %s", fullDeviceName, err))
		return httpPingResponse, err
	}
	return httpPingResponse, nil

}

/*
Return code:
0 - OK
1 - invalid configuration
2 - cached dsconfig configuration invalid
3 - json marshal error
4 - http new request error
5 - http post request error
6 - dsconfig configuration invalid
7 - dsconfig error
*/
func (r *DeviceServerReconciler) configureDevice(dcontext dtypes.OperatorContext, deviceserver tangov2.DeviceServer, req reconcile.Request, memcache *cache.Cache) (int, error) {
	log := r.Log.WithValues("configureDevice", deviceserver.Name)

	configContents, _, err := utils.CheckConfig(dcontext.Config)
	if err != nil {
		r.Recorder.Eventf(&deviceserver, corev1.EventTypeWarning, "Failed", "DeviceServer config is invalid: %q", deviceserver.Name)
		invalid_configuration.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
		reconcile_failures.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Inc()
		return 1, fmt.Errorf("config is invalid: %s", err.Error())
	}

	configureKey := fmt.Sprintf("%s.%s.configured", req.Namespace, req.Name)
	code, configured := memcache.Get(configureKey)
	Debugf(log, "Cache call says: %s => %s/%s = [%v] %s", configureKey, deviceserver.GetName(), deviceserver.Namespace, configured, code)
	if configured && code == "error" {
		return 2, fmt.Errorf("cached invalid configuration")
	}

	if configured {
		Debugf(log, "Cache said configured - skipping: %s/%s", deviceserver.GetName(), deviceserver.Namespace)
		return 0, nil
	}

	tango_host := fmt.Sprintf("%s.%s.svc.%s", dcontext.DatabaseDS, req.Namespace, dcontext.ClusterDomain)
	json2TangoUrl := fmt.Sprintf("https://%s/configuredb", dtypes.PingSvcUrl)
	values := map[string]string{"tango_host": tango_host, "port": fmt.Sprintf("%d", dcontext.Port), "config_content": configContents}
	json_data, err := json.Marshal(values)
	if err != nil {
		return 3, fmt.Errorf("configuredb json.Marshal error: %s", err.Error())
	}

	request, err := http.NewRequest("POST", json2TangoUrl, bytes.NewBuffer(json_data))
	if err != nil {
		return 4, fmt.Errorf("configuredb http.NewRequest error: %s", err.Error())
	}
	request.Header.Set("Content-Type", "application/json; charset=UTF-8")
	request.Header.Add("x-api-key", dtypes.ApiKeySecret)
	client := &http.Client{}
	postJson2TangoResponse, err := client.Do(request)
	if err != nil {
		return 5, fmt.Errorf("configuredb client.Do Error: %s", err.Error())
	}
	body, err := io.ReadAll(postJson2TangoResponse.Body)
	postJson2TangoResponse.Body.Close()
	if postJson2TangoResponse.StatusCode != 200 {
		if postJson2TangoResponse.StatusCode == 417 {
			memcache.Set(configureKey, "error", cache.DefaultExpiration)
			return 6, fmt.Errorf("configuredb error body: %s", body)
		}
		return 7, fmt.Errorf("configuredb error body: %s", body)
	}
	if err != nil {
		Errorf(log, err, fmt.Sprintf("configuredb Error: %s", err))
		return 8, fmt.Errorf("configuredb body read error: %s", err.Error())
	}
	memcache.Set(configureKey, "configured", cache.DefaultExpiration)
	return 0, nil
}

func (r *DeviceServerReconciler) getDeviceStates(pingUrl string, tango_host string, port int32, dsname string, instance string) (string, error) {
	return r.getDeviceRunningInfo(pingUrl, "states", tango_host, port, dsname, instance)
}

func (r *DeviceServerReconciler) getConfigFromDSConfig(pingUrl string, tango_host string, port int32, dsname string, instance string) (string, error) {
	return r.getDeviceRunningInfo(pingUrl, "config", tango_host, port, dsname, instance)
}

func (r *DeviceServerReconciler) getDeviceRunningInfo(pingUrl string, svc string, tango_host string, port int32, dsname string, instance string) (string, error) {
	log := r.Log.WithValues("getDeviceRunningInfo", dsname)
	url := fmt.Sprintf("https://%s/%s/%s/%d/%s/%s", pingUrl, svc, tango_host, port, dsname, instance)
	Debugf(log, "Url: %s", url)
	httpPingResponse, err := http.Get(url)
	if err != nil {
		Errorf(log, err, fmt.Sprintf("Url: %s", url))
		return "", err
	}
	body, err := io.ReadAll(httpPingResponse.Body)
	httpPingResponse.Body.Close()
	if httpPingResponse.StatusCode != 200 {
		Debugf(log, "Failed: Code: %d, Body: %s", httpPingResponse.StatusCode, body)
		return "", errors.New("httpPingResponse.StatusCode != 200")
	}
	if err != nil {
		Errorf(log, err, fmt.Sprintf("Url: %s", url))
		return "", err
	}
	var unescapedJSON string

	// Unmarshal the escaped JSON string
	err = json.Unmarshal(body, &unescapedJSON)
	if err != nil {
		Errorf(log, err, fmt.Sprintf("Url: %s", url))
		return "", err
	}

	Debugf(log, "Response Body: %s", string(body))
	Debugf(log, "Response Body Unescaped JSON: %s", unescapedJSON)

	return unescapedJSON, nil
}

/*
Return code:
0 - OK
1 - HTTP Ping Service Error: is reachable?
2 - Waiting
3 - Unable to fetch dependent DeviceServer
*/
func (r *DeviceServerReconciler) checkDependency(ctx context.Context, req reconcile.Request, dependency string, dcontext dtypes.OperatorContext, deviceserver tangov2.DeviceServer, retries int, memcache *cache.Cache, externalRetryKey string) (int, error) {
	log := r.Log.WithValues("checkDependency", deviceserver.Name)
	var dependentDeviceServer tangov2.DeviceServer
	dependentDeviceServerobjkey := client.ObjectKey{
		Namespace: req.Namespace,
		Name:      dependency,
	}

	is_valid_fqdn := validDeviceFQDN.MatchString(dependency)
	if is_valid_fqdn {
		httpPingSvcStatus := 200
		httpPingResponse, err := r.pingDevice(dtypes.PingSvcUrl, fmt.Sprintf("%s.%s.svc.%s:%d/%s", dcontext.DatabaseDS, req.Namespace, dcontext.ClusterDomain, dcontext.Port, dependency))
		httpPingSvcStatus = httpPingResponse.StatusCode
		if err != nil {
			Debugf(log, fmt.Sprintf("HTTP Ping Dev Error: %s/%s Waiting for %s (err: %s)", deviceserver.GetName(), deviceserver.Namespace, dependency, err))
			return 1, err
		}

		if httpPingSvcStatus != 200 {
			memcache.Set(externalRetryKey, fmt.Sprintf("%d", retries), cache.DefaultExpiration)
			return 2, nil
		} else {
			Debugf(log, fmt.Sprintf("dependent DeviceServer IS ready: %s - %s - %v", dependency, dependentDeviceServer.Status.State, dependentDeviceServer.Status.Resources))
			return 0, nil
		}
	} else {
		is_valid_hostport := validHostPort.MatchString(dependency)
		if is_valid_hostport {
			tmp_hp := strings.Split(dependency, ":")
			host := tmp_hp[0]
			port := tmp_hp[1]
			result_connection := r.RawConnect(host, port)
			if !result_connection {
				return 2, nil
			}
			return 0, nil
		} else {
			if err := r.Get(ctx, dependentDeviceServerobjkey, &dependentDeviceServer); err != nil {
				log.Error(err, "unable to fetch dependent DeviceServer(create/delete in progress?): "+err.Error())
				memcache.Set(externalRetryKey, fmt.Sprintf("%d", retries), cache.DefaultExpiration)
				return 3, err
			}

			if dependentDeviceServer.Status.State != "Running" {
				Debugf(log, fmt.Sprintf("Waiting for %s: %s - %v", dependency, dependentDeviceServer.Status.State, dependentDeviceServer.Status.Resources))
				memcache.Set(externalRetryKey, fmt.Sprintf("%d", retries), cache.DefaultExpiration)
				return 2, nil
			} else {
				Debugf(log, fmt.Sprintf("dependent DeviceServer IS ready: %s - %s - %v", dependency, dependentDeviceServer.Status.State, dependentDeviceServer.Status.Resources))
				return 0, nil
			}
		}
	}
}

/*
Return code:
0 - OK
1 - Waiting
2 - Unable to fetch dependent DeviceServer
3 - HTTP Ping service problem?
*/
func (r *DeviceServerReconciler) checkDatabaseDS(ctx context.Context, dcontext dtypes.OperatorContext, databasedsobjkey types.NamespacedName, databaseds tangov2.DatabaseDS, deviceserver tangov2.DeviceServer, req reconcile.Request, memcache *cache.Cache) (int, error) {
	log := r.Log.WithValues("checkDatabaseDS", deviceserver.Name)
	if !dcontext.DirectConnection {
		if err := r.Get(ctx, databasedsobjkey, &databaseds); err != nil {
			Debugf(log, "unable to fetch DatabaseDS(create/delete in progress?): "+err.Error())
			return 2, err
		}
	}

	if dcontext.DirectConnection {

		tango_host := strings.SplitN(dcontext.DatabaseDS, ":", 2)
		dcontext.DatabaseDS = tango_host[0]
		if len(tango_host) > 1 {
			_, err := fmt.Sscan(tango_host[1], &dcontext.Port)

			if err != nil {
				Errorf(log, err, fmt.Sprintf("Failed to parse Port from DatabaseDS: %s - %s", req.NamespacedName, deviceserver.Spec.DatabaseDS))
				dcontext.Port = 10000
			}
		}
	}

	if !dcontext.DirectConnection && (databaseds.Status.State != "Running" || databaseds.Status.Succeeded != 2) {
		Debugf(log, fmt.Sprintf("Waiting for DatabaseDS: %s - %s", deviceserver.Spec.DatabaseDS, databaseds.Status.State))
		return 1, nil
	} else {
		databaseDsConfKey := fmt.Sprintf("%s.%s.svc.%s.configured", dcontext.DatabaseDS, req.Namespace, dcontext.ClusterDomain)
		code, _ := memcache.Get(databaseDsConfKey)
		if code != "ready" {
			httpPingDSResponse, err := r.isDatabaseDSReady(dtypes.PingSvcUrl, fmt.Sprintf("%s.%s.svc.%s", dcontext.DatabaseDS, req.Namespace, dcontext.ClusterDomain), dcontext.Port)
			if err != nil {
				return 3, err
			}

			if httpPingDSResponse.StatusCode == 200 {
				memcache.Set(databaseDsConfKey, "ready", cache.DefaultExpiration)
				return 0, nil
			} else {
				return 1, nil
			}
		} else {
			return 0, nil
		}
	}
}

func (r *DeviceServerReconciler) setMetricsPullingTime(deviceserver tangov2.DeviceServer, req reconcile.Request, log logr.Logger) {
	var pod_name = fmt.Sprintf("ds-%s-0", deviceserver.Name)

	podEvents, err := r.getEvents(req.Namespace, pod_name, "Pod")
	if err != nil {
		return
	}

	found_pulled_time := false
	found_scheduled_time := false
	var pulled_time metav1.Time
	var scheduled_time metav1.Time
	for _, event := range podEvents.Items {
		Debugf(log, "Event: %s event.FirstTimestamp: %s (%s.%s)", event.Reason, event.FirstTimestamp.Time, deviceserver.GetName(), deviceserver.Namespace)
		if event.Reason == "Scheduled" {
			found_scheduled_time = true
			scheduled_time = event.CreationTimestamp
		}
		if event.Reason == "Pulled" {
			found_pulled_time = true
			pulled_time = event.FirstTimestamp
		}
	}
	Debugf(log, "image_pulling start/ready: %s/%s %s.%s", scheduled_time.Format(time.StampNano), pulled_time.Format(time.StampNano), deviceserver.GetName(), deviceserver.Namespace)

	if found_pulled_time && found_scheduled_time {
		pulling_time := pulled_time.Sub(scheduled_time.Time).Seconds()
		image_pulling.WithLabelValues(deviceserver.GetName(), deviceserver.Namespace).Set(pulling_time)
	}
}
