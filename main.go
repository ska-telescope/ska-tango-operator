/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/go-logr/logr"
	tangov1 "gitlab.com/ska-telescope/ska-tango-operator/api/v1"
	tangov2 "gitlab.com/ska-telescope/ska-tango-operator/api/v2"

	dtypes "gitlab.com/ska-telescope/ska-tango-operator/types"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"

	// extv1beta1 "k8s.io/api/extensions/v1beta1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	"go.uber.org/zap/zapcore"
	"k8s.io/apimachinery/pkg/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	metricsserver "sigs.k8s.io/controller-runtime/pkg/metrics/server"

	"gitlab.com/ska-telescope/ska-tango-operator/controllers"
	//+kubebuilder:scaffold:imports
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

// Debugf helper
func Debugf(log logr.Logger, format string, a ...interface{}) {
	log.V(1).Info(fmt.Sprintf(format, a...))
}

// Determine what the logging level was set tio
func getLevel() (bool, zapcore.Level) {
	// set logging level
	logLevelVar, ok := os.LookupEnv("LOG_LEVEL")
	// LOG_LEVEL not set, let's default to debug
	logDevelopment := false // will default to INFO
	logLevel := zapcore.DebugLevel

	if !ok {
		logDevelopment = true // Debug
	} else {
		switch strings.ToUpper(logLevelVar) {
		case "DEBUG":
			logDevelopment = true
			logLevel = zapcore.DebugLevel
		case "WARN":
			logDevelopment = false
			logLevel = zapcore.WarnLevel
		case "ERROR":
			logDevelopment = false
			logLevel = zapcore.ErrorLevel
		default:
			logDevelopment = false
			logLevel = zapcore.InfoLevel
		}
	}
	return logDevelopment, logLevel
}

func init() {
	_ = clientgoscheme.AddToScheme(scheme)
	_ = corev1.AddToScheme(scheme)
	_ = appsv1.AddToScheme(scheme)
	// _ = extv1beta1.AddToScheme(scheme)
	_ = tangov1.AddToScheme(scheme)
	_ = tangov2.AddToScheme(scheme)
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))
	utilruntime.Must(tangov1.AddToScheme(scheme))
	utilruntime.Must(tangov2.AddToScheme(scheme))
	//+kubebuilder:scaffold:scheme

}

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	var enableWebhooks bool
	var probeAddr string

	// flag.StringVar(&metricsAddr, "metrics-addr", ":8080", "The address the metric endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "enable-leader-election", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
	flag.BoolVar(&enableWebhooks, "enable-webhooks", false,
		"Enable WebHooks for the admission controller.")

	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "leader-elect", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")

	// set logging level
	logDevelopment, logLevel := getLevel()
	// ctrl.SetLogger(zap.New(func(o *zap.Options) {
	// 	o.Development = logDevelopment
	// 	o.Level = logLevel
	// }))

	// determine debug level
	if logLevel.String() == "debug" {
		flag.Lookup("v").Value.Set("1")
	} else {
		flag.Lookup("v").Value.Set("0")
	}
	flag.Parse()
	ctrl.Log.WithName("main").Info(fmt.Sprintf("v flag after: %+v", flag.Lookup("v")))
	intLevel, _ := strconv.Atoi(flag.Lookup("v").Value.String())
	if intLevel > 0 {
		logLevel = zapcore.DebugLevel
		logDevelopment = true
	} else {
		logLevel = zapcore.InfoLevel
	}
	ctrl.Log.WithName("main").Info(fmt.Sprintf("final logLevel: %+v", logLevel))
	ctrl.SetLogger(zap.New(func(o *zap.Options) {
		o.Development = logDevelopment
		o.Level = logLevel
	}))
	// ctrl.SetLogger(zap.New(func(o *zap.Options) {
	// 	o.Development = true
	// 	o.Level = zapcore.DebugLevel
	// }))

	log := ctrl.Log.WithName("controller-main").WithName("setup")
	// initialise core parameters
	image, ok := os.LookupEnv("DS_IMAGE")
	if !ok {
		dtypes.Image = "artefact.skao.int/ska-tango-examples:0.4.21"
	} else {
		dtypes.Image = image
	}
	Debugf(log, "Default DS Image: %s", dtypes.Image)

	image, ok = os.LookupEnv("DDS_IMAGE")
	if !ok {
		dtypes.DDSImage = "artefact.skao.int/ska-tango-images-tango-cpp:9.3.13"
	} else {
		dtypes.DDSImage = image
	}
	Debugf(log, "Default DDSImage: %s", dtypes.DDSImage)

	image, ok = os.LookupEnv("DB_IMAGE")
	if !ok {
		dtypes.DBImage = "artefact.skao.int/ska-tango-images-tango-db:10.4.19"
	} else {
		dtypes.DBImage = image
	}
	Debugf(log, "Default DBImage: %s", dtypes.DBImage)

	pullPolicy, ok := os.LookupEnv("PULL_POLICY")
	if !ok {
		dtypes.PullPolicy = "IfNotPresent"
	} else {
		dtypes.PullPolicy = pullPolicy
	}
	Debugf(log, "Default PullPolicy: %s", dtypes.PullPolicy)

	pingSvcUrl, ok := os.LookupEnv("PING_SVC_URL")
	if !ok {
		dtypes.PingSvcUrl = ""
	} else {
		dtypes.PingSvcUrl = pingSvcUrl
	}
	Debugf(log, "Default PingSvcUrl: %s", dtypes.PingSvcUrl)

	apiKeySecret, ok := os.LookupEnv("API_KEY")
	if !ok {
		dtypes.ApiKeySecret = ""
		log.V(1).Error(fmt.Errorf("Default apiKeySecret NOT provided"), "Default apiKeySecret NOT provided")
	} else {
		dtypes.ApiKeySecret = apiKeySecret
		Debugf(log, "Default apiKeySecret provided")
	}

	dtypes.MaxConcurrentReconciles = 5
	maxConcurrentReconciles, ok := os.LookupEnv("MAX_CONCURRENT_RECONCILES")
	if ok {
		i, _ := strconv.Atoi(maxConcurrentReconciles)
		if i > 0 {
			dtypes.MaxConcurrentReconciles = i
		}
	}

	Debugf(log, "Configure InsecureSkipVerify")
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	Debugf(log, "Default MaxConcurrentReconciles(%s): %d", maxConcurrentReconciles, dtypes.MaxConcurrentReconciles)

	Debugf(log, "Controller initialised.")

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme: scheme,
		Metrics: metricsserver.Options{
			BindAddress: metricsAddr},
		// Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "11dbded3.tango-controls.org",
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	if err = (&controllers.DeviceServerReconciler{
		Client:    mgr.GetClient(),
		Log:       ctrl.Log.WithName("controllers").WithName("DeviceServer"),
		CustomLog: dtypes.CustomLogger{Logger: ctrl.Log.WithName("controllers").WithName("DeviceServer")},
		Scheme:    mgr.GetScheme(),
		Recorder:  mgr.GetEventRecorderFor("tango-controller"),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "DeviceServer")
		os.Exit(1)
	}

	if enableWebhooks || os.Getenv("ENABLE_WEBHOOKS") == "true" {
		if err = (&tangov1.DeviceServer{}).SetupWebhookWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create webhook", "webhook", "DeviceServer")
			os.Exit(1)
		}
		if err = (&tangov2.DeviceServer{}).SetupWebhookWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create webhook", "webhook", "DeviceServer")
			os.Exit(1)
		}
	}

	if err = (&controllers.DatabaseDSReconciler{
		Client:    mgr.GetClient(),
		Log:       ctrl.Log.WithName("controllers").WithName("DatabaseDS"),
		CustomLog: dtypes.CustomLogger{Logger: ctrl.Log.WithName("controllers").WithName("DatabaseDS")},
		Scheme:    mgr.GetScheme(),
		Recorder:  mgr.GetEventRecorderFor("databaseds-controller"),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "DatabaseDS")
		os.Exit(1)
	}

	if enableWebhooks || os.Getenv("ENABLE_WEBHOOKS") == "true" {
		if err = (&tangov1.DatabaseDS{}).SetupWebhookWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create webhook", "webhook", "DatabaseDS")
			os.Exit(1)
		}
		if err = (&tangov2.DatabaseDS{}).SetupWebhookWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create webhook", "webhook", "DatabaseDS")
			os.Exit(1)
		}
	}

	// +kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
