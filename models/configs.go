/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package models

import (
	"encoding/json"

	"github.com/appscode/go/log"
	dtypes "gitlab.com/ska-telescope/ska-tango-operator/types"
	"gitlab.com/ska-telescope/ska-tango-operator/utils"
	corev1 "k8s.io/api/core/v1"
)

// DeviceServerConfigs generates the ConfigMap for
// the DeviceServer
func DeviceServerConfigs(dcontext dtypes.OperatorContext) (*corev1.ConfigMap, error) {

	const deviceserverConfigs = `
# Device Server configurations
apiVersion: v1
kind: ConfigMap
metadata:
    name: ds-configs-{{ .Name }}
    namespace: {{ .Namespace }}
    labels:
      app.kubernetes.io/name: ds-configs
      app.kubernetes.io/instance: "{{ .Name }}"
      app.kubernetes.io/managed-by: DeviceServerController
data:

  start-deviceserver.sh: |
    #!/usr/bin/env bash
    set -o errexit -o pipefail

    [ -f "${HOME}/.bash_profile" ] && source "${HOME}/.bash_profile"
    export TANGO_ZMQ_EVENT_PORT={{ .EventPort }} TANGO_ZMQ_HEARTBEAT_PORT={{ .HeartbeatPort }}

    {{ if .Command }}
    exec {{ .Command }} {{ .Args }} -ORBendPoint giop:tcp:{{ if ne .LegacyCompatibility true}}0.0.0.0{{ end }}:{{ .OrbPort }} -ORBendPointPublish giop:tcp:ds-{{ .Name }}.{{ .Namespace }}.svc.{{ .ClusterDomain }}:{{ .OrbPort }}
    {{ else }}
    {{ if .ScriptContents }}
    exec python3 /{{ .AppName }} {{ .Args }} -ORBendPoint giop:tcp:{{ if ne .LegacyCompatibility true}}0.0.0.0{{ end }}:{{ .OrbPort }} -ORBendPointPublish giop:tcp:ds-{{ .Name }}.{{ .Namespace }}.svc.{{ .ClusterDomain }}:{{ .OrbPort }}
    {{ else }}
    exec {{ if not (contains "python" .Script) }}python3 {{ end }}{{ .Script }} {{ .Args }} -ORBendPoint giop:tcp:{{ if ne .LegacyCompatibility true}}0.0.0.0{{ end }}:{{ .OrbPort }} -ORBendPointPublish giop:tcp:ds-{{ .Name }}.{{ .Namespace }}.svc.{{ .ClusterDomain }}:{{ .OrbPort }}
    {{ end }}
    {{ end }}

  {{ .AppName }}: |
{{ .ScriptContents | indent 4 }}


`
	result, err := utils.ApplyTemplate(deviceserverConfigs, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}
	configmap := &corev1.ConfigMap{}
	if err := json.Unmarshal([]byte(result), configmap); err != nil {
		return nil, err
	}
	return configmap, err
}

// DatabaseDSConfigs generates the ConfigMap for
// the DatabaseDS
func DatabaseDSConfigs(dcontext dtypes.OperatorContext) (*corev1.ConfigMap, error) {

	const databaseDSConfigs = `
apiVersion: v1
kind: ConfigMap
metadata:
  name: databaseds-configs-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: databaseds-configs
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DatabaseDSController
data:

  start-databaseds-tangodb.sh: |
    #!/usr/bin/env bash
    set -o errexit -o pipefail

    [ -f "${HOME}/.bash_profile" ] && source "${HOME}/.bash_profile"

    ls -latr /docker-entrypoint-initdb.d/*

    # exec /scripts/run.sh
    exec docker-entrypoint.sh mariadbd

  start-databaseds.sh: |
    #!/usr/bin/env bash
    set -o errexit -o pipefail

    [ -f "${HOME}/.bash_profile" ] && source "${HOME}/.bash_profile"

    exec /usr/local/bin/DataBaseds 2 -ORBendPoint giop:tcp::{{ .Port }} -ORBendPointPublish giop:tcp:{{ .DatabaseDS }}.{{ .Namespace }}.svc.{{ .ClusterDomain }}:{{ .Port }}


  config.json: |
{{ .ScriptContents | indent 4 }}

`
	result, err := utils.ApplyTemplate(databaseDSConfigs, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}
	configmap := &corev1.ConfigMap{}
	if err := json.Unmarshal([]byte(result), configmap); err != nil {
		return nil, err
	}
	return configmap, err
}
