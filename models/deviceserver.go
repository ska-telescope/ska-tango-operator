/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package models

import (
	"encoding/json"

	"github.com/appscode/go/log"
	dtypes "gitlab.com/ska-telescope/ska-tango-operator/types"
	"gitlab.com/ska-telescope/ska-tango-operator/utils"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
)

// DeviceServerService generates the Service description for
// the DeviceServer
func DeviceServerService(dcontext dtypes.OperatorContext) (*corev1.Service, error) {

	const deviceServerService = `
{{- $protected_labels := list "app.kubernetes.io/name" "app.kubernetes.io/instance" "app.kubernetes.io/managed-by" }}
{{- $protected_annotations := list "checksum/config" "kubectl.kubernetes.io/last-applied-configuration" }}
apiVersion: v1
kind: Service
metadata:
  name: ds-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: deviceserver
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DeviceServerController
{{- if .Metadata.Labels }}
{{- range $key, $value := .Metadata.Labels }}
{{- if not (has $key $protected_labels) }}
    {{ $key | toString }}: "{{ $value | toString }}"
{{- end }}
{{- end }}
{{- end }}
{{ if .Metadata.Annotations }}
  annotations:
{{- range $key, $value := .Metadata.Annotations }}
{{- if not (has $key $protected_annotations) }}
{{- if not (eq $key "vault.hashicorp.com/agent-inject-template-config") }}
    {{ $key | toString }}: "{{ $value | toString }}"
{{- else }}
    {{ $key | toString }}: {{ toYaml $value | indent 6 | trim }}
{{- end }}
{{- end }}
{{- end }}
{{- end }}
spec:
  selector:
    app.kubernetes.io/name:  deviceserver
    app.kubernetes.io/instance: "{{ .Name }}"
  type: {{ .ServiceType }}
  {{ if .ExternalDeviceServer }}
  externalName: {{ .ExternalDeviceServer }}
  {{ else }}
  ports:
  - name: tango-server
    port: {{ .OrbPort }}
    targetPort: tango-server
  - name: tango-heartbeat
    port: {{ .HeartbeatPort }}
    targetPort: tango-heartbeat
  - name: tango-event
    port: {{ .EventPort }}
    targetPort: tango-event
  {{ end }}
`
	result, err := utils.ApplyTemplate(deviceServerService, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}
	service := &corev1.Service{}
	if err := json.Unmarshal([]byte(result), service); err != nil {
		return nil, err
	}
	return service, err
}

// DeviceServerStatefulSet generates the StatefulSet description for
// the DeviceServer
func DeviceServerStatefulSet(dcontext dtypes.OperatorContext) (*appsv1.StatefulSet, error) {

	const deviceServerStatefulSet = `
{{- $protected_labels := list "app.kubernetes.io/name" "app.kubernetes.io/instance" "app.kubernetes.io/managed-by" }}
{{- $protected_annotations := list "checksum/config" "kubectl.kubernetes.io/last-applied-configuration" "json-dsconfig" "json-dsconfig-ret-time" "device-states" "device-states-ret-time" "metrics/dds-ready-at" "metrics/dependson-ready-at" "metrics/setupdb-ready-at"}}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: ds-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: deviceserver
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DeviceServerController
{{- if .Metadata.Labels }}
{{- range $key, $value := .Metadata.Labels }}
{{- if not (has $key $protected_labels) }}
    {{ $key | toString }}: "{{ $value | toString }}"
{{- end }}
{{- end }}
{{- end }}
  annotations:
    checksum/config: {{ .ConfigCheckSum }}
{{ if .Metadata.Annotations }}
{{- range $key, $value := .Metadata.Annotations }}
{{- if not (has $key $protected_annotations) }}
{{- if not (eq $key "vault.hashicorp.com/agent-inject-template-config") }}
    {{ $key | toString }}: "{{ $value | toString }}"
{{- else }}
    {{ $key | toString }}: {{ toYaml $value | indent 6 | trim }}
{{- end }}
{{- end }}
{{- end }}
{{- end }}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: deviceserver
      app.kubernetes.io/instance: "{{ .Name }}"
  replicas: 1
  template:
    metadata:
      labels:
        app.kubernetes.io/name: deviceserver
        app.kubernetes.io/instance: "{{ .Name }}"
        app.kubernetes.io/managed-by: DeviceServerController
{{- if .Metadata.Labels }}
{{- range $key, $value := .Metadata.Labels }}
{{- if not (has $key $protected_labels) }}
        {{ $key | toString }}: "{{ $value | toString }}"
{{- end }}
{{- end }}
{{- end }}
    spec:
      # PXH - disable until calico issue is resolved: https://github.com/kubernetes/kubernetes/issues/116632
      # and https://github.com/projectcalico/calico/issues/5715
      # serviceAccountName: "ds-serviceaccount-{{ .Name }}"
{{- with .PullSecrets }}
      imagePullSecrets:
{{- range $val := . }}
      - name: {{ $val.name }}
{{- end }}
{{- end }}
      containers:
      - name: deviceserver
        image: "{{ .Image }}"
        imagePullPolicy: "{{ .PullPolicy }}"
        command:
          - /start-deviceserver.sh
{{- if or (.PostStart) (.PreStop)  }}
        lifecycle:
{{- if .PostStart }}
          postStart:
            exec:
              command: ["/bin/sh", "-c", "{{ .PostStart }}"]
{{- end }}
{{- if .PreStop }}
          preStop:
            exec:
              command: ["/bin/sh","-c", "{{ .PreStop }}"]
{{- end }}
{{- end }}
        ports:
        - containerPort: {{ .OrbPort }}
          name: tango-server
        - containerPort: {{ .HeartbeatPort }}
          name: tango-heartbeat
        - containerPort: {{ .EventPort }}
          name: tango-event
        env:
        - name: TANGO_HOST
          value: {{ .DatabaseDS }}:{{ .Port }}
        - name: TANGO_ZMQ_EVENT_PORT
          value: "{{ .EventPort }}"
        - name: TANGO_ZMQ_HEARTBEAT_PORT
          value: "{{ .HeartbeatPort }}"
{{- with .Env }}
{{ toYaml . | indent 8 }}
{{- end }}
        volumeMounts:
        - mountPath: /start-deviceserver.sh
          subPath: start-deviceserver.sh
          name: ds-configuration
        - mountPath: /{{ .AppName }}
          subPath: {{ .AppName }}
          name: ds-configuration
{{- with .VolumeMounts }}
{{ toYaml . | indent 8 }}
{{- end }}
        readinessProbe:
          tcpSocket:
            port: {{ .OrbPort }}
          initialDelaySeconds: 1
          periodSeconds: 3
          timeoutSeconds: 3
          failureThreshold: 20
        livenessProbe:
          tcpSocket:
            port: {{ .OrbPort }}
          initialDelaySeconds: 3
          periodSeconds: 10
          timeoutSeconds: 3
          failureThreshold: 6
{{- with .Resources }}
        resources:
{{ toYaml . | indent 10 }}
{{- end }}
      volumes:
      - name: ds-configuration
        configMap:
          defaultMode: 0777
          name: "ds-configs-{{ .Name }}"
{{- with .Volumes }}
{{ toYaml . | indent 6 }}
{{- end }}
{{- with .NodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
{{- end }}
{{- with .Affinity }}
      affinity:
{{ toYaml . | indent 8 }}
{{- end }}
{{- with .Tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
{{- end }}

`

	result, err := utils.ApplyTemplate(deviceServerStatefulSet, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}

	deployment := &appsv1.StatefulSet{}
	if err := json.Unmarshal([]byte(result), deployment); err != nil {
		return nil, err
	}
	return deployment, err
}
