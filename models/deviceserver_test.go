package models

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	tangov2 "gitlab.com/ska-telescope/ska-tango-operator/api/v2"
	dtypes "gitlab.com/ska-telescope/ska-tango-operator/types"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
)

var _ = Describe("Generating a DeviceServer StatefulSet", func() {
	When("configuring resources", func() {
		It("should set CPU resources from the DeviceServer in the StatefulSet", func() {
			var cpuLimit = resource.MustParse("100m")
			var cpuRequest = resource.MustParse("50m")

			ds := tangov2.DeviceServer{
				Spec: tangov2.DeviceServerSpec{
					Resources: &corev1.ResourceRequirements{
						Limits: corev1.ResourceList{
							corev1.ResourceCPU: cpuLimit,
						},
						Requests: corev1.ResourceList{
							corev1.ResourceCPU: cpuRequest,
						},
					},
				},
			}

			dcontext := dtypes.SetConfig(ds)
			dcontext.Metadata = ds.ObjectMeta
			statefulSet, err := DeviceServerStatefulSet(dcontext)

			Expect(err).NotTo(HaveOccurred(), "failed to create test StatefulSet resource")

			statefulSetResources := statefulSet.Spec.Template.Spec.Containers[0].Resources
			Expect(*statefulSetResources.Limits.Cpu()).To(Equal(cpuLimit))
			Expect(*statefulSetResources.Requests.Cpu()).To(Equal(cpuRequest))
		})

		It("should set memory resources from the DeviceServer in the StatefulSet", func() {
			var memoryLimit = resource.MustParse("1Gi")
			var memoryRequest = resource.MustParse("500Mi")

			ds := tangov2.DeviceServer{
				Spec: tangov2.DeviceServerSpec{
					Resources: &corev1.ResourceRequirements{
						Limits: corev1.ResourceList{
							corev1.ResourceMemory: memoryLimit,
						},
						Requests: corev1.ResourceList{
							corev1.ResourceMemory: memoryRequest,
						},
					},
				},
			}

			dcontext := dtypes.SetConfig(ds)
			dcontext.Metadata = ds.ObjectMeta
			statefulSet, err := DeviceServerStatefulSet(dcontext)

			Expect(err).NotTo(HaveOccurred(), "failed to create test StatefulSet resource")

			statefulSetResources := statefulSet.Spec.Template.Spec.Containers[0].Resources
			Expect(*statefulSetResources.Limits.Memory()).To(Equal(memoryLimit))
			Expect(*statefulSetResources.Requests.Memory()).To(Equal(memoryRequest))
		})

		It("should set other resources from the DeviceServer in the StatefulSet", func() {
			var fpgaResourceName = corev1.ResourceName("fpga-vendor.example/fpga")
			var fpgaLimit = resource.MustParse("2")
			var fpgaRequest = resource.MustParse("1")

			ds := tangov2.DeviceServer{
				Spec: tangov2.DeviceServerSpec{
					Resources: &corev1.ResourceRequirements{
						Limits: corev1.ResourceList{
							fpgaResourceName: fpgaLimit,
						},
						Requests: corev1.ResourceList{
							fpgaResourceName: fpgaRequest,
						},
					},
				},
			}

			dcontext := dtypes.SetConfig(ds)
			dcontext.Metadata = ds.ObjectMeta
			statefulSet, err := DeviceServerStatefulSet(dcontext)

			Expect(err).NotTo(HaveOccurred(), "failed to create test StatefulSet resource")

			statefulSetResources := statefulSet.Spec.Template.Spec.Containers[0].Resources
			Expect(statefulSetResources.Limits[fpgaResourceName]).To(Equal(fpgaLimit))
			Expect(statefulSetResources.Requests[fpgaResourceName]).To(Equal(fpgaRequest))
		})
	})
})
