/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package models

import (
	"encoding/json"

	"github.com/appscode/go/log"
	dtypes "gitlab.com/ska-telescope/ska-tango-operator/types"
	"gitlab.com/ska-telescope/ska-tango-operator/utils"
	networkingv1 "k8s.io/api/networking/v1"
)

// DNSNetworkPolicy generates the NetworkPolicy description for
// the DNS for all resources
func DNSNetworkPolicy(dcontext dtypes.OperatorContext) (*networkingv1.NetworkPolicy, error) {
	const dnsNetworkPolicy = `
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: ds-networkpolicy-dns-{{ .Name }}
  namespace: {{ .Namespace }}
  labels:
    app.kubernetes.io/name: ds-networkpolicy-dns
    app.kubernetes.io/instance: "{{ .Name }}"
    app.kubernetes.io/managed-by: DeviceServerController
spec:
  podSelector:
    matchLabels:
      app.kubernetes.io/managed-by: DeviceServerController
      app.kubernetes.io/instance: "{{ .Name }}"
  policyTypes:
  - Egress
  egress:
  # enable the entire DeviceServer cluster to talk to DNS
  - ports:
    - port: 53
      protocol: UDP
    - port: 53
      protocol: TCP
`

	result, err := utils.ApplyTemplate(dnsNetworkPolicy, dcontext)
	if err != nil {
		log.Debugf("ApplyTemplate Error: %+v\n", err)
		return nil, err
	}

	networkpolicy := &networkingv1.NetworkPolicy{}
	if err := json.Unmarshal([]byte(result), networkpolicy); err != nil {
		return nil, err
	}
	return networkpolicy, err
}
