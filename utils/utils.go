/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

// Package utils contains the utility functions for the tango-operator
package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path/filepath"
	"regexp"
	"strings"
	"text/template"

	"github.com/Masterminds/sprig"
	"github.com/go-logr/logr"
	dtypes "gitlab.com/ska-telescope/ska-tango-operator/types"

	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/helm/pkg/chartutil"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
	ctrl "sigs.k8s.io/controller-runtime"
)

// Errorf helper
func Errorf(log logr.Logger, err error, format string, a ...interface{}) {
	log.Error(err, fmt.Sprintf(format, a...))
}

// Infof helper
func Infof(log logr.Logger, format string, a ...interface{}) {
	log.Info(fmt.Sprintf(format, a...))
}

// Debugf helper
func Debugf(log logr.Logger, format string, a ...interface{}) {
	log.V(1).Info(fmt.Sprintf(format, a...))
}

// recursive call to convert map keys
func ConvertKeysToString(i interface{}) interface{} {
	switch x := i.(type) {
	case map[interface{}]interface{}:
		m2 := map[string]interface{}{}
		for k, v := range x {
			m2[k.(string)] = ConvertKeysToString(v)
		}
		return m2
	case []interface{}:
		for i, v := range x {
			x[i] = ConvertKeysToString(v)
		}
	}
	return i
}

// YamlToMap converts a YAML string to a JSON string
// The input string is a byte array, and output is a generic interface{} struct
func YamlToMap(s string) (interface{}, error) {
	log.Debugf("YamlToMap Input: %s", s)
	var body interface{}
	if err := yaml.Unmarshal([]byte(s), &body); err != nil {
		return nil, err
	}

	body = ConvertKeysToString(body)
	log.Debugf("YamlToMap Parsed: %+v", body)
	return body, nil
}

// FuncMap taken from Helm
func FuncMap() template.FuncMap {
	f := sprig.TxtFuncMap()
	delete(f, "env")
	delete(f, "expandenv")

	// Add some extra functionality
	extra := template.FuncMap{
		"toToml":   chartutil.ToToml,
		"toYaml":   chartutil.ToYaml,
		"fromYaml": chartutil.FromYaml,
		"toJson":   chartutil.ToJson,
		"fromJson": chartutil.FromJson,

		// This is a placeholder for the "include" function, which is
		// late-bound to a template. By declaring it here, we preserve the
		// integrity of the linter.
		"include":  func(string, interface{}) string { return "not implemented" },
		"required": func(string, interface{}) interface{} { return "not implemented" },
		"tpl":      func(string, interface{}) interface{} { return "not implemented" },
	}

	for k, v := range extra {
		f[k] = v
	}

	return f
}

// ApplyTemplate take YAML template and interpolates context vars
// then returns JSON
func ApplyTemplate(s string, context dtypes.OperatorContext) (string, error) {

	log := ctrl.Log.WithName("controllers")
	t := template.Must(template.New("template").Funcs(FuncMap()).Parse(s))
	var tpl bytes.Buffer
	if err := t.Execute(&tpl, context); err != nil {
		Debugf(log, "Template Error: %+v\n", err)
		return "", err
	}

	yamlData := tpl.String()
	Debugf(log, "Rendered Template: %s\n", yamlData)
	body, err := YamlToMap(yamlData)
	if err != nil {
		Debugf(log, "YamlToMap Error: %+v\n", err)
		return "", err
	}
	// the YAML de-serialiser is rubbish so we have to wash it through
	// the JSON one!!!
	result, err := json.Marshal(body)
	if err != nil {
		Debugf(log, "json.Marshal Error: %+v\n", err)
		return "", err
	}

	Debugf(log, "Output: %s\n", result)
	return string(result), nil
}

// CheckScript - check what sort of script has been passed to the DaskJob
func CheckScript(script string, dsname string) (string, string, bool, error) {
	// check Script - is it a script, file or URL
	var (
		scriptName    string
		scriptContent string
		mountedFile   bool
	)
	scriptName = dsname + ".py"
	scriptContent = ""
	mountedFile = false
	matched, _ := regexp.MatchString(`(?m)^#!\/usr\/bin\/env python\n`, script)
	if matched != true {
		// string is not a valid py script - it must be a file
		// an unknown local file
		mountedFile = true
	} else {
		// has #!python line, so it's probably a script
		scriptContent = script
	}

	return scriptName, scriptContent, mountedFile, nil
}

// CheckConfig - check what sort of config has been passed to the DeviceServerJob
func CheckConfig(config string) (string, bool, error) {
	// check Config - is it a JSON config, file or URL
	var (
		configContent string
		mountedFile   bool
		out           interface{}
	)
	configContent = ""
	mountedFile = false

	// short circuit on empty config
	if config == "" {
		return configContent, mountedFile, nil
	}

	err := json.Unmarshal([]byte(config), &out)
	if err != nil {
		// string is not a valid py script - check for URl and file
		u, err := url.Parse(config)
		if err != nil {
			// not a valid URL or filename - final test, throw out this Job
			return "", mountedFile, fmt.Errorf("Cannot determine config - json, URL or file: %s#", config)
		} else {
			ext := strings.Replace(filepath.Ext(u.Path), ".", "", -1)
			if ext != "json" {
				return "", mountedFile, fmt.Errorf("Cannot determine config (suffix) - json, URL or file: %s#%s", config, ext)
			}
			if u.Scheme == "http" || u.Scheme == "https" {
				resp, err := http.Get(config)
				if err != nil {
					return "", mountedFile, fmt.Errorf("Cannot resolve config (%s)", config)
				} else {
					defer resp.Body.Close()
					body, err := io.ReadAll(resp.Body)
					if err != nil {
						return "", mountedFile, fmt.Errorf("Failed to HTTP get config (%s)", config)
					} else {
						configContent = string(body)
					}
				}
			} else {
				// an unknown local file
				mountedFile = true
			}
		}
	} else {
		// valid JSON, so it's probably a notebook
		configContent = config
	}

	return configContent, mountedFile, nil
}

func validateScheduleFormat(schedule string, fldPath *field.Path) *field.Error {
	// if _, err := cron.ParseStandard(schedule); err != nil {
	// 	return field.Invalid(fldPath, schedule, err.Error())
	// }
	return nil
}
